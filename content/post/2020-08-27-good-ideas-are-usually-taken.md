---
title: Good ideas are usually taken
date: 2020-08-27
tags: ["ideas", "random thoughts"]
---

I had a thought recently, why is it that when I think of something *obvious* that *no* one else has ever done, it seems like the world has just *missed* some awesome, *obvious*, thing?

Then I look into it, and discover that it *is* a thing, or at least ***was*** a thing.

I got to thinking about it more, and I decided that most ideas that are obvious, yet something you've never heard of before, falls into one of these categories:

1. It's a good idea and it's popular already but you didn't do enough research to discover it, so you think it's a new idea when it really isn't.
1. It's unpopular because it has a fatal flaw you haven't thought of yet.
1. It's popular in small circles, so it's hard to find/research, but it's already popular in the places it needs to be.
1. It's truly revolutionary and you are sitting on a golden novel idea.

This is why it's important if you are relying on this idea to build a new business. It's best to do as much research as possible to figure out where you are in this idea. Usually though, I find there is a fatal flaw I haven't thought of yet. It's very easy to poke holes in other peoples ideas, but harder to poke holes in your own ideas. Getting good at scrutinizing your own ideas will enable you to find truly *unique* good ideas.

Most of the time, it's just a fun thought experiment. Scrutinizing your own ideas allows you to avoid your own ignorance.


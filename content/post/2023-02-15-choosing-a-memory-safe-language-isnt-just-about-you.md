---
title: Choosing a memory safe language isn't just about you
date: 2023-02-15
tags: ["Rust", "Software", "C++", "Architecture", "Languages", "theory"]
---

# Why are you talking about this?

I'm not a prolific blogger, but when I get that itch to write something down, I put it here. Today I want to talk about memory safety, Rust, C++, and other languages that compare themselves to the C family of languages in speed and development type.

In short, I'm talking about this because it's on my mind.

# What's the deal?

Rust advertised itself as being a new programming language that was fast and efficient. It wowed people with abstractions that optimized down to speedy code. But many languages can claim to have these features, Rust was different in a more fundamental day. The ownership and borrowing nature of Rust allowed the compiler, with it's borrow checker, to ensure that you, the programmer, won't have certain fundamental memory problems if you pass the compiler checks.

That's pretty sweet, and certainly something unique to Rust.

But not all is well in the community of software developers, because we know that memory issues from C and C++ have caused problems and headaches and security breeches for years. Rust brought a solution to these problems, but it wasn't alone. C++ has a plethora of new solutions that has helped developers with these common bugs. In theory, if you write C++ carefully, you get as many guarantees as Rust.

# Let's read that back carefully

`In theory, if you write C++ carefully, you get as many guarantees as Rust.`

Oooh, there's some wiggle words in there. "In theory", "carefully", "Rust" (Wait, not that one). This means that if you add a bunch of tooling around C++ you can, yes YOU can, write safe and efficient C++.

And if you were the only one using your software, you may be convinced. You might just stop there and say "Yeah, I did due diligence, I'm confident this is good code. This is hard work, but I did the work. My code is safe and I'm confident of that".

But I'm not (Sorry for switching first person like that).

# Don't be mean Brian

Okay, let me rephrase that. If you write Rust, I know, exactly, the types of compiler errors you see, because I write Rust as well. I know you had to deal with ownership and borrowing on a fundamental level. I KNOW you passed the compiler (Unless you cheated with `unsafe`, but that is a simple recursive grep away from being checked), which means I KNOW your code is memory safe.

But the C++ compiler doesn't have these checks, you need linters, static analysis tools, pair programming or a really good code reviewer, etc. to catch these issues.

Sure, maybe *you* are confident in you, but *I'm* not.

In short, It's not you, It's me. But seriously, the reason Rust is still a better tool for writing memory safe code, is because unless you cheat, you automatically got memory safety. With C++, you automatically get *unsafe* code, and you have to try really hard to get memory safe code. I just trust the Rust compiler more than a person. Well, when it comes to compiling memory safe code anyway. If you wanna recommend a good burger in a new town, I'd trust you. Compilers don't even eat food.

# What about open source code

Awesome, let's review a good argument for why you *could* trust C++ code to be memory safe. If it's open source, I can always download the source myself, run linters and static analysis tools, and check for myself.

Hooray for open source!

But alas, most people don't do this. Of course it's done sometimes, there are such things as 3rd party security reviews and such. But today, I don't care to download everyone's source code, run a bunch of linters and static analysis tools, and post these in ranked order from best to worst categorized by what the code does. That's a lot of effort.

But then again, Rust already did that for me, well, not the ranking, but the safety portion. If you passed the Rust compiler, you get a check+!

# But Rust isn't perfect!

Okay, so there is the `unsafe` keyword, and yeah, there are abstract types that float memory issues into runtime instead of compile time. But, and I'm not defending every aspect of Rust here, there are such things as trade offs. In the end, Rust strikes the best balance for the kind of code Rust targets. You can write bad code in any language, but at least Rust will panic instead of crashing, at least it won't double free, at least it won't leak memory (It's still technically possible, but much harder). There's just more you can count on.

# A parting thought

Don't let perfect be the enemy of good. Rust is good, not perfect, but C++ is full of compromises and foot guns. Of course you should *not* rewrite every C++ app in Rust. But maybe, just maybe, understand why Rust isn't just about *you* writing better code for *you*, but for your users, your consumers, to better trust you did a good job.


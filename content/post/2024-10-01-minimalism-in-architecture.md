---
title: Minimalism in architecture, startups, and vertical integration
date: 2024-10-01
tags: ["software engineering", "architecture", "theory"]
---

# Finding the Balance Between Cloud Services, Local Hardware, and Software Design

I've been thinking a lot about minimalism lately—not just in architecture, but in software development and design too. I don't have all the answers, but some experiences and things I've read have got me thinking. So I wanted to share my thoughts on the whole cloud vs. local hardware debate and how software design choices play into this, especially when considering company size and growth.

## Cloud vs. Local Hardware: Small vs. Big Companies

For small companies, the cloud seems like a no-brainer. It lets you get up and running quickly without huge upfront costs. You can spin up resources as you need them and pay only for what you use. But even as a small company, there are times when using your own hardware makes more sense because it's cheaper in the long run.

I read an article where someone was using a cloud service for a simple lambda function—a small process that runs, does its thing, and shuts down. They found it was expensive and a bit slow. So they whipped up a bash script on their laptop that did the same job faster, cheaper, and with way less code. It's obvious to me that we've overutilized the cloud to do minimal workloads that can be best done locally, or with other kinds of cloud workloads.

What about AI workloads that need GPUs? You could spend $20,000 on a beefy computer loaded with high-end GPUs. You get immediate, fast access to all that hardware, storage, and memory—all in one machine. Writing code that taps into the CPU, RAM, disk, and GPUs is straightforward because it's all right there.

In the cloud, you can spin up ten GPUs in minutes without that hefty upfront cost. You pay by the minute or even the second. If you're only using the GPUs occasionally, it's cheaper. But if you're running them all the time, cloud costs add up fast. Some services cost so much that if you're using them more than 10-20% of the time, it's more expensive than just buying the hardware. Plus, after paying all that money, you don't own anything at the end—turn off the service, and it's gone. For a startup, renting hardware becomes a liability, whereas if you buy something, even if it depreciates, it's an asset.

So even as a small company, sometimes owning hardware makes more sense. Medium to big companies might lean more towards owning hardware because, at scale, the savings are significant. A managed 20-terabyte cloud database might cost around $5,000 per month. Sure, you get automatic backups, replication, recovery, upgrades—the whole package. But for that same $5,000 per month, you could buy way more than 20 terabytes of storage. You can get 20 TB for ~$500, you could get backups in triplicate, and replace them every 6 months, and still save 10's of thousands per year. I just in the last 5 minutes did a quick search and configred a storage server with 240 TB (Terrabytes!) of storage with a stupid fast Xeon processor and 256 GB (Gigabytes!) of ram for $12,500. This thing can run Raid 6 and get 200 TB of storage. After 6 months you would spend $30,000 on cloud storage for 20 TB. At that rate you can afford a new storage server *just to backup your first server*, and **still save money by comparison**, and you have * **10x the space** *... Of course, managing that storage takes time and expertise, which isn't free. A dedicated database admin might cost $150,000 to $200,000 a year when you factor in everything. That makes the $60,000 a year for the cloud database seem like a bargain, especially if you don't have the resources to handle it in-house. At that rate the cost of the server is next to nothing, saving one person's salary as a startup saves you more money than all your hardware and cloud costs combined for the year... Or like... 10 years (Not a made up number, that's the *worst* case scenario money saved by my calculations in the first couple years of a startup with mostly minimal cloud costs. When running at decently reasonable scale, on person is still about 1 year worth of cloud costs...).

## Software Design Choices: Longevity vs. Immediate Impact

This balance also applies to software design. Right now, tools like React and Web Components are super popular. If I put out a call for developers who know these tools, I'd get tons of applicants. But I've worked with these tools and seen companies rewrite their applications every 1-3 years. That's kinda ridiculous. Why would you need to rewrite your app so often?

Frameworks like React are moving targets. You can build something quickly, but the patterns and best practices change so fast that your code becomes outdated. New developers might come in and say the app needs a rewrite because it's not up to current standards. As a startup, I can't afford to rewrite my entire UI or backend every couple of years. I want to build something that lasts. (Short note: Some people might say that this is an updated concept and React has slowed down in development as of lately. Broadly, I agree, but compared to other tools, it still cycles out really fast. How long does Django or Ruby on Rails last, even if you used React as a framework that you deployed? And ultimately, Golang, with its built-in HTML and web tools and its standard library, will probably last at least a decade. You could still decide to deploy React, but that part of your application will inevitably get rewritten. Anything in Rust/Go, hell, even Java, will likely outlast a React app)

So do I pick tools that are more stable and long-lasting? The problem is, those tools aren't as popular. I won't find thousands of developers who know them—maybe just a few dozen. But maybe that's okay if I can find a handful of good developers. In fact, I've read a lot about startups that have picked Haskell or Erlang or something more esoteric as their language of choice. This leads to a very small pool of developers, but they are significantly higher quality. It's almost like the choice leads to an initial filter for higher quality developers. If I ask for a bunch of front-end developers who write in Golang, I'm not going to get that many, but I think I'm going to get people who align with me philosophically on my minimalism software long-term stability approach.

The catch is that newer fast-moving tools often come with easy-to-buildslick interfaces that users expect. Building a nice UI with less common tools might take longer. So on one hand, I could build a fancy interface in six months using popular tools but risk needing a rewrite soon. On the other hand, I could spend a year building something more stable that'll last but take longer to get to market.

Maybe the tradeoff is in how much I build. Perhaps I use a slower, more stable method but scale back on features to get a usable product out the door faster.

## Tradeoffs and Company Growth

Ultimately, small companies have to make these tradeoffs. We need to balance immediate impact with long-term sustainability. Big companies, on the other hand, have different priorities. They've moved past the initial technical hurdles and are now focused on reducing costs as much as possible and getting as many users as they can.

For big companies, it might not matter if they rewrite the app every year. As long as they're building something new and fresh and telling people about it, the tradeoff is worth it. They'll probably attract more users. Having an entire department dedicated to tools you might buy off the shelf is totally worth it because of the reduction in costs at scale.

This is similar to vertical integration in other industries. When you start off, you buy materials from suppliers. But as you grow, it might make sense to buy the supplier itself. In manufacturing, a company might start by buying steel from a producer, but eventually, they'll buy the mine that produces the steel. This way, they're not paying another company's markup.

But the whole idea behind vertical integration is to reduce costs as much as possible, which can harm the people behind those tools or materials. In software, once you start consolidating resources and creating internal monopolies, the focus shifts to making services as cheap as possible, sometimes at the expense of quality or ethics.

## This Isn't New, I'm Just Able To Get Serious About It Now

A lot of this stuff has been written about before, but now I have to grapple with it myself. These are just my thoughts poured out on paper, trying to make sense of the tradeoffs and find a path forward that aligns with both practical needs and my own values.

---
title: How to reverse a file with vim macros
date: 2019-11-22
tags: ["vim", "tricks", "terminal"]
---

# Who is this for?

Anyone who's interested in vim macros, but aren't sure exactly how to use them, or what they might be useful for.

You will come across a gluttony of solutions to the challenges you face in writing code and notes. Everyone wants to sell you something, either with free speech software, free beer software, free-while-we-steel-your-data software, or even non-free software (_**gasp**_). The trick in navigating all these "Solutions" is to determine which ones you **actually** need.

Sometimes, you need the exact hammer for the nail you want to drive. Sometimes, a swiss army knife will work just fine, especially if you're already trained in how to use it. Vim is the swiss army knife of all knives, and macros are just a way to play back vim keys in a defined order, automating tedious tasks.

# First, how do vim macros work?

Okay, so this is a primer on vim macros. I'm assuming you know enough of vim to navigate around a file, search, delete lines and paste them, etc.. So let's jump into the `q` button in vim.

When you are in normal mode in vim, and hit the `q` button, nothing happens (As the unix philosophy goes, no news is good news). The next button you hit, we'll use `w` since it's right there, tells the user `recording @w`. Aha! Something did happen! We are now recording. From here on out, every button you hit will end up in a recorded buffer, until you hit `q` again. Then an `@w` will repeat what you put into the buffer. Let's see an example.

Say you want to change a file with the current contents

```text
hello
hello
```

to be 

```text
world
world
```

Let's assume we are at coordinates 0,0 after we open the file in vim. We will then type everything here, in order:

`<esc>qwdwiworld<esc>qbj@w<esc>:wq`

It looks like this:

<script id="asciicast-P4gy3VLtr5KIEzfjZZ3ktBKGu" src="https://asciinema.org/a/P4gy3VLtr5KIEzfjZZ3ktBKGu.js" async></script>

Let's break down the commands:

`<esc>` # Make sure we are in normal mode (You can always mash escape 20 times before starting, but that's just me :)

`qw` # Begin recording, save this recording to the "w" key

-- In the recording ---

`dw` # Delete the word

`iworld` # Enter insert mode, type the word "world"

`<esc>` # Go back to normal mode

`q` #  End recording

-- After the recording --

`bj` # Go back a word, go down a line

`@w` #  Replay what was saved in "w"

`<esc>:wq` # go back to normal mode, save and quit. Since what was recorded was `dwiworld<esc>`, the word will be deleted, and the characters 'world' are entered directly. Effectively copying what we did before. If we needed to do this 1,000 times, it would be worth it to set this up.

That's a really simple example, but I think it gets the point across.

# The challenge

So I actually had this problem recently. I keep a daily log of everything I do. One of my logs has been going on for months, and I decided to write it in reverse order, like so:

```text
  1  Some quick notes
  .
  .
 10 === Date (newest entry)
 11 . A thing
 12 . Another thing
  .
  .
 20 === Date (oldest entry)
 21 . A thing
 22 . Another thing
 23
 24 == 2019
```

It seemed like a good idea at the time. Every time I open the file, the first thing I see is what I did recently, and the further down the file, the further back you go in history.

The problem is, admittingly, pretty first world. I had a handful of static notes at the top of the file, and it was _inconvenient_ to type `34j` to go pas that top section to begin writing another day's log. I'd rather type `G` to go to the bottom of the file, it's faster and easier. Also reading my history was weird, I had to read down the log for a particular day, but then scroll up to read the next entry, _this had to be fixed_. The file was also nearly 500 lines, too long to copy/paste each day by hand, as that would be far to error prone.

Okay so this is a silly problem, but it was also fun to figure out that it was a really straightforward solution with vim macros to reverse the log. Hey, a challenge is a challenge.

# How do you reverse a simple file?

Alright, let's reverse a file, `reverseme.txt` like this:

```text
a
b
c
```

to instead ready:

```text
c
b
a
```

Okay, how about this:

```bash
$ sort -r reverseme.txt
```

Well crap, looks like this problem is to easy to solve. No vim macros required at all.

Ah, it's a trick! That example was in alphabetical order, what about this?:

```text
you
can't
reverse
this
by
sorting
```

Turns out, unix-y systems are pretty cool, theres this other command:

```bash
$ tac -r reverseme.txt
```

The first line for the tac man page says:

`tac - concatenate and print files in reverse`

Okay first lesson here: Research the systems you use, because a **lot** of times, there actually are perfect hammers to your nails built in. Sometimes, the simples solution is the best one.

BUT WAIT! That wasn't the challenge. I didn't have to reverse the whole file line by line, I had to keep a day's worth of logs in order, but change the order of the days around. That's pretty different. I'm sure someone out there has a nifty awk/sed/perl script to do this out of the box, but I don't, so I used my swiss army knife.

# How did I solve the real problem?

My strategy started because I realized it was only a handful of commands to copy a day of log notes to the bottom of the file. Because it was formatted in asciidoc, I had some syntax I could take advantage of. Each day was started with `===`, which denotes a level 2 heading in asciidoc. The year marker had `==`, a heading 1. This meant I could use the double equals as an anchor, I needed to move all triple equals sections to below the double equals sections. That was important, because I could search for headings and my anchor in vim using `/^=== ` and `/^== `.

At a high level, the plan went something like this:

<img src="/post/images/move-days-below-an-anchor.jpg" width=250>

I could denote a "day" by the `===`, and the anchor by the `==`, like here:

```text

... more days above

=== Tuesday
. A thing
. Another thing
. A third thing

=== Monday
. A thing
. Another thing
. A third thing

== 2019

```

So the plan was to start the macro recording on Monday, copy and paste the day to the end of the file using `/` to search and jump between sections, then navigate back to Tuesday. Then when I run the macro again, back to back, it copies Tuesday next.

Let's see what' this macro looks like:

`<esc>/^== <enter>?^=== <enter>qwV/^== <enter>kxGp/^== <enter>?^=== <enter>q`

That's a complicated one, let's break it down like before.

`<esc>` # Enter normal mode

`/^== <enter>` # Find the anchor, only one line starts with `==` in the whole file

`?^=== <enter>` # Find the first line **above** the anchor that starts with `===`

-- Everything above was just to find the first day we want to copy, now let's set up the recording --

`qw` # begin recording

`V` # enter highlight line mode (Notice the capital `V`, not a lowercase `v`)

`/^== <enter>k` # Find the anchor (`==` sign), go one line above that. This is important because we could have any number of log items for a particular day, since don't know how many lines it is, we can just copy every line until the anchor-1.

`x` # cut that section

`Gp` # go to the end of the file, paste it.

`/^== <enter>` # Find the anchor again. We need to do this again because after every paste, we will have a different number of lines above us to get back to the next day to copy. Searching is much more effective.

`?^=== <enter>q` # go to the first day above the anchor, stop recording. We stop recording here, because at the beginning of the recording, we started highlighting, then cut/paste. Letting ourselves stop here means we can run the macro back to back without any extra navigation. Critical to save a lot of time.

Wow that's a lot of stuff! The important thing is that what we've cut will _always_ be the last day before the anchor, no matter how many items are in that day, and pasted them to the end of the file, no matter how much is below us already. We will always return to the last day above the anchor, which means we can run this macro back to back to keep moving days down. In fact, we can combine macros with the repeater key. so while in normal mode, `6@w` will repeat the macro saved on the 'w' key 6 times. If we manually move Friday, then copy that macro 6 times, every day of the week will copy. Let's see this in action:

<script id="asciicast-AkmTvDsTlpu26KW0CXePSreyC" src="https://asciinema.org/a/AkmTvDsTlpu26KW0CXePSreyC.js" async></script>

So here is the things with vim macros, you can set up a macro to do something simple, like in the first example, or you can set it up to do really crazy things that save you tons of time. The time vs value proposition is based on how long it takes to make the macro, how many times you will use it, and how much time you save every time you use it. In my case I had a hundred+ days I wanted to copy around. It took a good 5 minutes to experiment, fail, and finally set up the macro correctly. It saved me much more time than that, not to mention the possibility of cut/pasting something incorrectly. In the end, macros are a tiny little program that just runs what you typed in order. If you take the time to set up the macro right, you will easily reap the benefits.

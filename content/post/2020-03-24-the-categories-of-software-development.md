---
title: The categories of software development
date: 2020-01-15
tags: ["software", "engineering", "architecture"]
---

# The series

1. [Part 0: The intro]({{< ref "2020-03-24-the-real-challenges-of-software-engineering-series.md" >}})
1. [Part 1: The categories and common terminology I'll use (This part)]({{< ref "2020-03-24-the-categories-of-software-development.md" >}})
1. [Part 2: Marketing and conversation is focused on the wrong areas]({{< ref "2020-03-24-organization-spend-time-in-the-wrong-areas.md" >}})
1. [Part 3: Only good design will save you from these challenges]({{< ref "2020-03-24-only-good-design-saves-you.md" >}})
1. [Part 4: DevOps won't save you from your challenges]({{< ref "2021-02-10-devops-wont-save-you.md" >}})
1. [Part 5: CICD won't save you from your challenges]({{< ref "2021-02-11-cicd-wont-save-you.md" >}})
1. [Part 6: The final answer to good software devlopment]({{< ref "2022-01-26-the-final-answer-to-good-software-development.md" >}})

# The categories of software development

When a developer sits down to work on software, they generally have one of 6 goals in mind. Keep in mind there are no hard limits between these categories, gray areas do exist.

1. Build software with minimal dependencies
2. Build software with dependencies on a major framework or library
3. Build software with internal dependencies
4. Fix code that was built wrong
5. Change code to do different things
6. Change how code works, but keep all functionality the same

Let us understand these categories a bit more.

## Category 1: New code with obvious dependencies

Obvious dependencies include the language itself, the standard library, and pretty much anything built in, or very commonly used. Standard I/O tools fall under this category, but probably not database interfaces.

You can teach anyone how to program in this category. CS students sit here for about a year. I'm teaching my 10 year old nephew how to write functions in the Python turtle library (I'm not counting turtle, it's just a teaching tool). Simple stuff like that belong here.

## Category 2: New code dependent on frameworks and libraries

By this I mean dependent on major dependencies, such as a framework you are working in, a database interface, a GUI library, a runtime framework, etc. Being depending on such large frameworks, libraries, and API's are common, but must be considered carefully in software development.

Things in this category include server software, GUI frameworks, major database interfaces or libraries, and major frameworks that encompass your entire project, such as Spring.

## Category 3: New code that depends on your own code

Obviously at some  point in a project you will write some code that gets used in many places, an interface you use to make some functionality generic, a service or API your app uses internally, or even a whole framework you developed internally. At this point you really get to see the pain points of your own design (Unless you have good tests, in which case you've found these pain points already, we'll come back to tests in a later post).

## Category 4: Modify code that was written wrong

This is bug fixing. Clearly we aren't perfect people and writing code is difficult. At some point you will realize you wrote something wrong, or something didn't work exactly the way it was intended. It's also possible there is a security vulnerability that changes the way a feature is implemented. In this case you need to modify code to function as originally intended.

## Category 5: Change code

At some point in time, you will realize that some functionality deemed necessary earlier in the project is no longer needed. This code needs to be changed, or possibly removed. It was implemented correctly in the first place, but requirements have moved beyond its needs.

## Category 6: Change how code works, but keep the functionality the same

At the heart of this is performance optimizations, simplifications, or security enhancements. You want your feature set to stay the same, but either make the code run faster, make it more secure, or make it easier to understand.

# Wrap up

That's all for this post. A short explanation of the different categories and terminologies of software development I'll be using in later posts.

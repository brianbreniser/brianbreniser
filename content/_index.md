# Hello friends

This is my public space for sharing my thoughts, experiences, and publicly available information about me. It contains topics around software development, IT, DevOps, my hobbies, or any random thoughts I would like to put down into words.

Regarding my life in general. I find myself to be a generalist. I like being good at many things. I like having multiple hobbies I can move in-between. Additionaly to work and hobbies, my wife and I welcomed our first child into the world in mid 2022.

You can read more about me in the *about* section above. Or below you will find my blog, which I write occasionally.

Thanks for stopping by!


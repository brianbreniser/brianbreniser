---
title: Only good design can save you from bad design
date: 2020-01-16
tags: ["software", "engineering", "architecture"]
---

# The series

1. [Part 0: The intro]({{< ref "2020-03-24-the-real-challenges-of-software-engineering-series.md" >}})
1. [Part 1: The categories and common terminology I'll use]({{< ref "2020-03-24-the-categories-of-software-development.md" >}})
1. [Part 2: Marketing and conversation is focused on the wrong areas]({{< ref "2020-03-24-organization-spend-time-in-the-wrong-areas.md" >}})
1. [Part 3: Only good design will save you from these challenges (This part)]({{< ref "2020-03-24-only-good-design-saves-you.md" >}})
1. [Part 4: DevOps won't save you from your challenges]({{< ref "2021-02-10-devops-wont-save-you.md" >}})
1. [Part 5: CICD won't save you from your challenges]({{< ref "2021-02-11-cicd-wont-save-you.md" >}})
1. [Part 6: The final answer to good software devlopment]({{< ref "2022-01-26-the-final-answer-to-good-software-development.md" >}})

Check out the [software development categories]({{< ref "2020-03-24-the-categories-of-software-development.md" >}}) before reading this, It's a short read and will make a lot more sense.

# I have the solution, but you won't like it

I'll start at the solution, the real way that you can make your development life easier. Writing code is hard and will never be easy, but the fact of the matter is we make it much worse on ourselves than we need too.

The solution is good architects that you trust to design good software, and good developers that will put time and effort into making sure their implementation is maintainable. These architects and developers don't come cheap. They need to work together, between them and with their managers, to ensure the system is evolving in the right direction. Managers will need to trust them when they say something is unrealistic, and developers need to accept when managers give them a list of priorities. With this strategy, you will not get an initial great burst of software up front, but you will also be able to consistently expand on your project for quite some time.

Normally, at this point someone is going to try to sell you something. Either literally software you can buy with money, or a seminar that will teach you something. Maybe someone will try to convince you to adopt a certain development methodology, or an open source tool to solve all your problems.

There is no such thing. Unfortunately, you can't buy a product to save yourself from this. You need better code design, and good developers to design your code.

What code design is best? It depends.

What patterns should we follow? It depends.

What services should we adopt? It depends.

What software libraries will make our code better? It depends.

This is a job for architects, engineers, and developers, that get paid good money to make your lives easier. Even if you are a developer yourself, it's nice to have a second opinion on these solutions.

This is where companies skimp out, they don't hire good talent that understands how to design code well, for some definition of *well*.

No one on the internet can tell you how to make your project better. No one can tell you the best way to build your software. An architect or experienced engineer, on the other hand, someone part of your team for a long time (Or just starting out, but who really cares), who has investments in your software (Not monetary, but time-wise), someone who has seen your company culture, is going to have the best idea of how to proceed for you.

The hard part about making recommendations online is that there are no universal answers. Some software works great for some uses, but not for others. Sometimes libraries will work out of the box, your project might be 90% done before you get to start. Rarely this is the case, and you usually need to build a lot more than 10% of your overall code base. Only someone with experience in many areas can tell you which libraries, methodologies, etc. will work best for you. The best software requires good code design, that is implemented by good developers, that requires good architects for guidance and support.

# Well that wasn't a satisfying answer

I know, but in another article I will give more references to the types of things someone can learn that will make them better at designing software for long term growth, or at least less short term pain.

---
title: Organizations spend time on the wrong parts of software development
date: 2020-01-16
tags: ["software engineering"]
---

# The series

1. [Part 0: The intro]({{< ref "2020-03-24-the-real-challenges-of-software-engineering-series.md" >}})
1. [Part 1: The categories and common terminology I'll use]({{< ref "2020-03-24-the-categories-of-software-development.md" >}})
1. [Part 2: Marketing and conversation is focused on the wrong areas (This part)]({{< ref "2020-03-24-organization-spend-time-in-the-wrong-areas.md" >}})
1. [Part 3: Only good design will save you from these challenges]({{< ref "2020-03-24-only-good-design-saves-you.md" >}})
1. [Part 4: DevOps won't save you from your challenges]({{< ref "2021-02-10-devops-wont-save-you.md" >}})
1. [Part 5: CICD won't save you from your challenges]({{< ref "2021-02-11-cicd-wont-save-you.md" >}})
1. [Part 6: The final answer to good software devlopment]({{< ref "2022-01-26-the-final-answer-to-good-software-development.md" >}})

Check out the [software development categories]({{< ref "2020-03-24-the-categories-of-software-development.md" >}}) before reading this, It's a short read and will make a lot more sense.

# TLDR

Don't have all day? No problem:

Teams spend the majority of their focus in category 2, the language, libraries, and frameworks, but the majority of the difficulties of software development are actually about building decoupled software that is easy to change, falling in categories 4, 5 and 6. The only solution to bad code I've ever seen is good quality architecture and high quality (Read: expensive) developers.

## Okay I have time for more details

Cool:

The vast majority of products for software developers missed the point of why software development is hard. People want you to use their language, library, or framework (A category 1 or 2 decision), but none of that **really** matters. The difficult part of software development is *changing* software. Adding features, bug fixing, changing features, and optimizations. Categories 3, 4, 5, and 6. No one can sell you a fix for this, it just requires *good* developers. *Good* developers are expensive, and companies tend to avoid that.

# Let's walk though a typical project

A project starts in categories 1 and 2. Fresh code is made either in a purist business logic way, or a project might start by deciding a major dependency to use, such as a major GUI framework or server framework to adhere too.

This is easy. You can write code with little problems, even test it thoroughly, you don't expect many bugs, no features need to be changed, performance can be worried about later, security isn't a problem yet, and even when you depend on your own code, it's still limited in functionality and features. You are spending 95% of your time in categories 1, 2, and *maybe* 3.

Life is easy.

But when a project gets big, and "big" means something different for every project, then you sit pretty much exclusively in areas 3-6, that's when it gets hard.

For example, let's say you have your minimal set of features done. You can test it with a test audience, and get some feedback. That's great, you now have some notes, tickets, feature requests, and maybe a bug or two. You are able to knock those out pretty quickly. You continue working on your backlog of work. You go through more rounds of reviews, getting more feedback, more users, and more bugs. You're working 50/50 on backlog issues and user feedback. Now you get more, more feature requests, more bugs, and now performance optimizations, etc. Things shift pretty quickly. Now, you are spending all your time fixing bugs, changing features, and optimizing. You spend all your time in categories 4-6, and spend a little time on 3, and seldom, if any, on 1 and 2.

Let's visualize this with a graph:

<img src="/post/images/work_category_burndown_drawn_graph.jpg" width=800>

The left side represents coding work capacity. The bottom represents time. Over time, you add more categories of work, starting with framework code and simple features, moving on to internally dependent code, fixing bugs, changing features, and finally performance and security enhancements that were originally left behind.

The graph is generic, the time is unitless, and we ignore time for planning and meetings and day to day fluff. But in the end, it is useful for visualizing why we slow down. At first, we feel like we can go fast, but over time, we have new categories of work we need to include day to day.

This is the core of my argument. If we can minimize bugs, make changing features easy, and pre-emptively plan for performance and security, then we will remain productive for longer. Frameworks, libraries, and language selection, are decided early, and we spend a small amount of time in those categories overall when the project is no longer new. A framework could be so bad that it hurts your performance, but don't let someone oversell you on one. Spend your time and money wisely.

If you are a startup, then hopefully you become profitable before the work gets really hard, because if you aren't shipping the right features before adding features becomes too time consuming, you will fail. Otherwise, for larger organizations with plenty of money and no real software strategy, they can basically plow through the infinitely difficult (And expensive) work ahead.

# This is fine

Some of you out there might remark on this: "Hey, this is normal, this is fine, this is how it works".

But I still don't want to be a [boiling frog](https://en.wikipedia.org/wiki/Boiling_frog).

<img src="/post/images/dog_on_fire_this_is_fine.jpg" width=500>

I like this quote [“A smart person learns from his mistakes, but a truly wise person learns from the mistakes of others.”](https://www.goodreads.com/quotes/564321-a-smart-person-learns-from-his-mistakes-but-a-truly). Just because these problems are normal, doesn't mean we can't do something about it.

But let's rant a little. Why is it that **EVERYONE** focuses on topics 1 and 2?

"Use my language"

"Use my framework"

"Use my runtime engine"

We fight about it all the time, we have so many [opinions that people wrong songs about it](https://www.youtube.com/watch?v=yqTpG5obPV8).

# New languages aren't actually a problem

I'm not against new languages. I like seeing new ideas about how to write programs. I think about languages all the time. It's distracting actually, scribbling down the categories that languages fall in. languages I like, ones I don't, ones I want to try. But I know now that when it comes down to true challenges in software development, language choice matters less than I originally thought.

The problem is we focus on new languages to solve the wrong problems.

I like Rust, I like Go, I like Elm, I like new languages. But really, they don't make our projects easier to build. There's a handful of real "features" that that differentiate languages. Things that actually matter in languages are things like garbage collection, objects, how functions work, variable mutability, if it has the goto keyword or not, if it interpreted or not. After that, it's all the same, really. New languages can be fun to learn, and of course we have trends that new languages can take advantage of, but most languages are pretty much the same.

# A real language difference

Let's use an example, Rust vs C. Rust doesn't have a garbage collector, but it does ensure you don't have certain memory issues at compile time. Awesome, sounds like writing in Rust will reduce the amount of bugs you have. After all the bickering and comparing, you can at least say there's certain bugs that C has that Rust doesn't have. that's because C and Rust have real differences.

# But most languages are the same

What about Python vs Ruby? Both interpreted, mutable, not purely functional, no goto, and have flexible variable typing. They are basically the same language. Sure, I like Python more than Ruby, but for no reason other than I was introduced to it first. You can't say that Python is better than Ruby or the other way around. Their popularity is simply a function of market trends. Ruby was popular for making websites, but Javascript frameworks have usurped Ruby in that area. Python is an old language, but recently has been adopted for the machine learning crowd, making it's popularity continue for the foreseeable future.

Similarly, how different are Javascript frameworks? Sure, new frameworks take advantage of new trends, some are based on async functionality, some try to adhere to natural language terminology, some try to be "minimal", "full-featured", or "modern". Really though, they are all Javascript based, they all have basically the same goals, they are just swapping trendiness.

The fact of the matter is that our websites aren't hard to maintain because we picked the wrong framework, the problem is that we never prioritized how to approach bug fixing, feature additions, feature removal, performance optimizations, and security enhancements, which are where we spend the vast majority of our time once we have some real code working.

# A "real" problem

Ever heard of tech debt? That's when we're trying to add features, but bugs, other poorly implemented features, our own API's, etc. are getting in the way. We want to do something simple, category 3 work, but categories 4, 5, and 6 take up all of our time. Adding features is easy at first, while working in a vacuum or a fresh project, but depending on our own stuff gets hard after even a moderate amount of code is written. The fact of the matter is that the easier it is for code to change, the easier it is to fix bugs and change old features, add security, and tweak performance. Then, and *only* then, will you be able to ensure you are able to build features consistently over time.

# My code is too small to have these issues

There is no definition of project size that can or can't run into these kinds of problems. Theoretically, a well designed system will fend off tech debt falling into categories 4, 5, and 6, well enough that a project could go on for years and millions of lines without development becoming too slow. However, a poorly designed project will likely run into issues very early in the process. No one can predict exactly when these issues will arise, but it doesn't have to take long.

In fact, there will always be an acceptable level of difficulty in this area, development *is* difficult after all. The real question is, how much can your organization handle? If you are a startup, you really need to make money, so long as you can stay ahead of the problems well enough to keep making a profit, all is well. For larger companies, a slow moving and slow evolving product might work well enough. With enough money and patience, even the most grossly slow development might be something you can put up with. It's all about the context of the project.

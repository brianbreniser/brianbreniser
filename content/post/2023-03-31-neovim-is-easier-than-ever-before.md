---
title: Neovim is Easier Than Ever Before!
date: 2023-03-31
tags: ["Neovim", "Rust", "IDE", "editors", "chatgpt"]
---

## Forewarning about A.I.

A portion of this article was written by Chat GPT-4. Those sections are clearly outlined. Every other word outside of those clearly marked sections was written 100% by me.

# My early days of using VIM

I was taught vim in college, because I was part of the college of engineering's IT support staff. I learned how to use all the old school tools like vim, and Linux, and the command line, etc. Since my programming classes primarily used Linux, and they supported our use of vim, I use it very heavily early on in my career.

In my first few jobs in software development, I really wanted to use vim for my development environment. But I got a lot of pushback from these groups because, at the time, vim did not have a lot of support for new languages, new frameworks, testing, debugging, and other features that something like Intellij had right out of the box. No one wanted to "support" my use, becuase it would mean having documentation around multiple editors. I tried to convince them that I didn't need that, I only needed project specific documentation, and I could make vim work, but it didn't work. It's true that vim just didn't support everything, so I would have to split up my tools into vim, a debugger, and even an external tester. I knew there were ways of making vim do it all, but it was heavily conviluted and difficult to set up and maintain.

# Neovim now

This is no longer true, it may have been true back in the day, but thanks to neovim, nearly 100% of the features the people want out of an IDE can be achieved with either very little configuration, or no configuration at all. However, I still wanted to build neovim to work the way I wanted, and so I still maintain quite a bit of custom configuration.

I discovered this because I'm a consultant now and I don't use all of the same tools consistently like I used to. I'm always using different tools based on the customer I'm working with. When I decided to start on a project outside of a customer, I wanted to write it in rust and using the latest rust development tools. I immediately went for VS code because it is typically considered to have the best rust development experience. And it did! That is, until I ran into a bug in the rust analyzer. I debugged that for two whole days, I even asked for answers on rust forums. Nobody knew what was going on. As it turns out, it was probably just a bug relating to a point release that messed up some file imports. The long story short of that is that I wasn't the only one that had this bug, but it was caused by a point release that was released that morning, and so nobody had done the work to figure out the fix yet. So, I went on the hunt for the next best thing for developing Rust. I knew that rust in vim was getting better. So I decided to search for how to do rust with the rust analyzer in vim. I pretty quickly ran into neovim and realized that it was going to be much better solution, and not just for Rust, but as a generic IDE that can match nearly any IDE in features, and blow them away in easier configuration. (By the way, that project is temporarily here: [financialplanner.breniser.me](http://financialplanner.breniser.me))

# It's not just me

Boy was I right, neovim has become the next big thing in text editors, and IDEs. Neovim has gathered first class support for the majority of new plug-ins being built. I rarely find something brand new that hasn't been implemented in Intellij, VSCode, and Neovim, right off the bat. Even github Copilot and Copilot X, support neovim as a first class citizen.

# Even AI agrees with me

This section on the evolution of Neovim what was produced by chat GPT-4.

ChatGPT-4:
>Neovim, a modernized fork of the classic Vim text editor, has undergone significant transformations over the years to cater to the evolving needs of developers. Its primary goal is to facilitate a more enjoyable and efficient coding experience. Neovim boasts an enhanced plugin architecture, asynchronous input/output processing, and improved scripting capabilities that collectively contribute to its prowess as a first-class IDE.

>One of the most significant milestones in Neovim's development was the implementation of the Language Server Protocol (LSP). The LSP allows developers to integrate language-specific features such as code completion, diagnostics, and refactoring into Neovim without having to develop and maintain language-specific plugins. This has been instrumental in making Neovim a highly extensible and versatile IDE.

> Many popular languages, such as JavaScript, TypeScript, Python, Rust, Go, and C++, have adopted the LSP, further solidifying Neovim's status as a first-class IDE. The LSP has not only made it simpler for language maintainers to support Neovim, but it has also made it easier for developers to work with multiple languages within the same development environment.

I couldn't have said it better myself, which is why I just inserted that verbatim. The LSP has allowed neovim to be a first class citizen at the same rank level as any other IDE.

Here's a quote from chat GPT-4 about some of the most popular plug-ins that people use to make neovim look and feel more like a common IDE:

ChatGPT-4:
>As Neovim continues to gain traction among developers, plugin maintainers have been quick to recognize its potential and prioritize its support. The community has developed a plethora of plugins that enhance Neovim's functionality, making it a highly customizable IDE capable of rivaling the likes of IntelliJ and VSCode.

>Some notable plugins that have transformed Neovim into a first-class IDE include:

> - coc.nvim: This plugin brings IntelliSense-like features to Neovim, offering a comprehensive development experience akin to that of VSCode. It provides features such as auto-completion, diagnostics, and code actions, among others.
> - nvim-treesitter: This plugin leverages the power of Tree-sitter, a parsing library, to enable advanced language-aware editing features such as syntax highlighting, code folding, and text object manipulation.
> - fzf.vim: This plugin integrates the FZF fuzzy finder into Neovim, allowing developers to quickly search and navigate through files, buffers, tags, and more.
> - vim-fugitive: A powerful Git integration plugin for Neovim, vim-fugitive, offers a comprehensive suite of Git commands and features within the editor, streamlining version control tasks.
> - vim-test: This plugin simplifies the testing process by enabling developers to run tests for various test frameworks directly from within Neovim.

> The extensive plugin ecosystem has played a pivotal role in making Neovim a first-class IDE, offering a rich set of features that cater to diverse programming needs.

# A.I. is cheating

OK, so I was the one that wrote the prompt to tell chat GPT-4 to talk favorably about neovim. I asked it to generate text on why neovim is now considered a first class interactive development environment, and to highlight the fact that plug-in maintainers consider it a first class citizen.

# Well... I'm still impressed

The Brian from six years ago would be very happy to see the progress that neovim brought to developing in vim. I think it is time for most teams to understand that other people want to have different environments to program in from themselves. There's more than one player in town, and a good developer as a connection to their tools in a way that helps them reason about what they're building. Sometimes it's about going faster, sometimes it's about focusing on typing instead of mouse movements, sometimes it's just nice to be able to open up a file, edit one line of text, and all of a sudden a new keyboard binding works. And it's just more of a pain in the butt to navigate through a series of settings that feel convoluted.

But of course, being that we all have different opinions, it's fine to allow us to use different editors. What I value isn't the same as what other's value. And that is just fine.

I still think that vim, via neovim, is no longer just a nerd tool with tons of compromise. Used only for the sake of telling everyone that you're better because you use the harder tool to configure. It's really not that hard to set up a neovim config anymore. Using Lua as a config language, along with plenty of starter templates and tutorials on how to configure neovim make this easier than it's ever been before. If you still think it's hard, that's okay! I emphasise that we are all different, that that is good. Maybe though, just don't get made when I use neovim at work?

# Okay one last pitch

Learning the vim modes and key bindings isn't horribly difficult if you put a little time into it. Once you get good at modal editing, you will likely find that there are *major* advantages. You can speed up your development, or at very least, just keep your hands on your keyboard more. Configuring a neovim instance is easier than it's ever been. So I suggest to just try it. I think you'll find that it's come along way, and I think you'll find that you've got all of the support from all of the tools and plug-ins that you need at your disposal.


---
title: Don't panic after a back injury
date: 2019-11-22
tags: ["freethought", "weightlifting", "short"]
---

# Today I hurt my back

I was lifting this morning doing my usual weekly progression program, but I was feeling quite tired, not quite 100%. I had already failed my 3x5 overhead press bump in weight, and deadlift was next. I was doing 1x5 at 90% of my 1x5 weight for deadlift. After 2 reps my arms were quite tired, so I switched from overhand grip to mixed grip. The first lift was fine, but the second felt wrong. My lower right back popped. Not a small amount of pain jumped through my back. I dropped the weight, crossed my arms, and just stood there, thinking about what to do.

I channeled [Dr. Austin Baraki](https://www.barbellmedicine.com/the-team/dr-austin-baraki/), thinking to myself that before I panic, I need to assess the situation.

Test: Can I walk? Yes.

Test: Can I bend over without passing out? Yes.

So I walked around for a minute, I needed the rest anyway. Once I felt comfortable, I did some air deadlifts and squats, it felt uncomfortable, but possible. I took off the excess weights and left 135 on the bar. I proceeded to do slow deadlifts. I was testing myself, showing my body I wasn't really all that hurt. I did multiple sets of deadlifts, some rows, some stiff leg deadlifts, all to get some blood pumping in my back and prove to myself it wasn't bad.

In the end, the pop was probably nothing major, I walked away from the gym normal. This afternoon it just feels as if I had a heavy day of lifting, a bit sore but manageable, I bet tomorrow it will be a bit stiff, no big deal.

It kind of went like this youtube video:

[I HURT MY BACK! What to do now](https://youtu.be/riq-DfDDimc)

But bear in mind I am NOT as strong as Alan is, my deadlift at 90% was 290 lbs. Pain like this can happen at every level. It also didn't seem as bad. I was able to recover to do 135 on the bar the first day. Alan waited until the next day to put on weight. The theory is the same though, a back injury does not have to lead to a week of couch sitting and feeling bad for yourself.

The biggest thing I kept in mind was something Alan Thrall said: "The more I moved, the better it felt".

Alan refers to Austin Baraki's article, it's [here](https://startingstrength.com/article/aches-and-pains) for those interested.

Update: 12/18/2019

A fortuitous video from Jordan Feigenbaum was released recently that discusses how lower back pain is not caused by only one thing. The bio-psycho-social model is a better description of [lower back pain](https://www.youtube.com/watch?v=l9poXGU11ms&t=191s).

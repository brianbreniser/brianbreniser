---
title: How do you make a good software development team
date: 2023-02-19
tags: ["software", "engineering", "hiring", "teams", "developers"]
---

My blog is about 4 years old, because of this, some of my oldest posts that held opinions of mind might be out of date. I have new thoughts and new ways of describing my old thoughts.

This post talks about something I've written in-depth about before. If you want to see the full 6 part series (Yikes!), you can start here:

1. [Part 0: The intro]({{< ref "2020-03-24-the-real-challenges-of-software-engineering-series.md" >}})

But that isn't needed to understand this post. This post stands on its own. If this post intrigues you, feel free to check out the 6 part series.

# A short introduction

Software development is hard.

That's an understatement.

# But why is it hard?

It's hard for a couple of reasons. I'll come up with the simplest explanation now.

There's two categories of reasons software development is hard. One from the developers side, one from the managers side. Let's break down the developers first, then the managers second.

(Please note, this applies mostly to developing web-apps, front ends, back ends, etc.).

## Developers

Developers are pressured in many ways to focus on the wrong aspects of software development. It's *SO* easy to find loads of tutorials, blogs, online classes, etc. on learning one language, one framework, one piece of a tech stack, just one thing. Traditional software developers learned how computers worked. We studied operating systems, algorithms, low level programming languages that taught you about memory and pointers. Nowadays, most languages we use today have memory management, and come with algorithm libraries and pre-build frameworks. Old school developers still know how these systems work, and that can be important. If all you learn is how *Awesome framework (R)* works, then that's all you can work on.

So here is the deal...

Most of the difficult problems in programming are not language or framework related. They are 1) Designing a coherent system that can be maintained over time (Architecture) and 1.5) Maintaining your high level of development over time 2) Fixing errors you might have written back when the project was older (Bug fixing) 3) Performance enhancements (This never gets done, like literally never. Don't even worry about it on a normal team because you will never have time to make an app performant).

So there ya go. Developers are pressured to learn about framework code, which is irrelevant in the long run of an application. Development is hard because of logical errors and complicated systems, and we just don't focus on that. Over time developers care less and less about learning about theory, and ultimately can't solve many problems. The more we google, the less we know, the more we abstract, the less we need to know.

## Managers

Okay but managers don't always help the problem either. Let's think about this. Most mangers want to hire *MANY* developers (Excluding startups or companies with a small tech side). They don't want to have to vet every developer for real knowledge about programming. Testing logic and systems is hard, and even error prone, as programming is actually kind of a creative skill and you can't always pull your best out of a hat.

So what do you ask for on a resume? You ask if the developer has experience in the language, frameworks, and operating systems they use.

Ooops.

I just got done saying that programming is hard because of logical errors, not framework code. In fact, even when framework code is a problem, it's something good developers can't even do anything about because of the layers of abstraction. We *ALL* just google those errors. But real programming is hard because of logic, something no manager can adequately test for, or can adequately hire for.

The feedback system has failed. It's pushed more and more people to care about irrelevant things. You can hire 1000 React developers, but none of them know how to program around Reacts failings. None of them will know what to do when you decide to move away from React. If you hire only for frameworks, you will end up cycling all your developers in 10 years, max.

# I'm not convinced

Fair enough, the plural of anecdote is not data, but software engineering isn't studied with formal methods often anyway. So let's look at our personal examples.

I assume *YOU* are on a software development team, you produce software, or you are on a team that produce software. If you feel you or your team suffers from a lot of slowdowns, problems, buggy code, etc. Then I ask you to think about your tech stack.

Using React? Angular? Springboot? Golang? Kubernetes? Openshift? AWS? Pgsql? Kotlin on Android? Swift on Apple machines?

Who makes these? Some are open source, some are made by the biggest tech companies in the world. Apple, Amazon, Facebook, Twitter, Google, etc.

What about your team? You can't build a PoC web form in under 3 weeks? How about Google? Trillion dollar company that came out with the first real alternative to Microsoft office. You both use Angular, so what's the difference here?

Your team? Having trouble pulling 1000 lines of data from a database and display them to your webapp with React in under 5 seconds? Facebook? They run Facebook on react (Well, and Php). So what's the difference. It's not the frameworks.

You can copy all these tech companies tech stacks all you want, but in the end, is it making anything easier? It's not making you billions of dollars. It's certainly not making your software faster.

Okay, let's break down some differences. Google has LOTS of data, so much data that even if you had the same tech they have, you just can't replicate their software. For example, Google made google voice, which recorded everyone's phone messages, then used those recordings to convert audio to text, and feed that into a machine learning algorithms. Even if you had the same algorithm, you don't have the training data. You can't replicate their voice to text AI.

Also, influence, Google made google docs which competes with Office, could you? Maybe, but probably not. There are open source alternatives to Office, like Libre office. It's good, but not popular. Google has a name, so when they made docs, and attached it to gmail for free, they could spread their software like wildfire.

Apple controls their entire stack, you can't make an iphone any better than apple. Making ANY new phone is hard. Essential did it with the essential phone, and they died after one release, and didn't even make a new operatin system, they just used Android. It's hard to do these complicated things without already being a multi billion dollar company.

So, it's not apples to apples.

But, if your team can't make a simple website that is a simple form with less than 10 docker containers running with $300/mo of compute resources, then your architects, developers, and managers, may have missed what's important in software development.

# Okay fine, I'm still listening, so, where are these solutions you promised?

Well... Here's the things. I know the theory behind the solutions, but I don't know all the details. I think we need to get back on track to finding out a better way to teach software, to hire developers, and to prioritize work.

We also need to simplify our platforms, sometimes our code sucks and it's actually not our fault, but the fault of complicated systems and abstractions. Sometimes, Google and Amazon and Apple aren't creating software that's faster because they have better developers, but because they are already rich enough to throw billions of dollars into parallel resources. They do this because, well, it's all slow and sucky, and they have enough money to make the problem go away.

But you can't spend all your time fixing bad platforms, you need a software team today (Or, soon). So what can you start on now to make your teams better?

## 1. Hire good developers/architects

Okay let's just dump *right* into the hardest part first. You need to hire smart people. Let me explain what that means.

You need to hire people you trust.

How do you vet for that? Well if you knew that, you could make serious money as an independent consultant. For now, let's talk about what trust means. Architects design your systems, they see the big picture. They develop too, but they can zoom out.

Okay, some important details about architects. Do they say no? If your architect never says no, you know they can't be trusted. A good architect knows when something should not be done, maybe it's not possible, or maybe it will just cost too much. Your architect should know your budget and timeline, and you can set those all you want, but the architect should be able to tell you what's possible in those constraints. If they can't, or if they are just a yes person, they aren't trustworthy.

Your architects should give high quality estimates, as in, they should never say something will be done by X date. Software *DOES NOT WORK LIKE THAT*. You can NOT build software by a certain date, you can only do work as best as you can by that date. Architects that give dates are doing one of two things. One, they are giving dates so far out that they know they can get done with the job days or weeks or months ahead of time, and then secretly hold back releasing until the desired date. Or two, they are just guessing, missing the target as often as they hit it. This isn't good, this isn't information, this is over-confidence with a blind eye. The best you can do is a date *range*, with varying levels of confidence (Between January and March, with 80% confidence). The absolute best you can do is admit that dates are the wrong way to do software. Trust your project managers and designers to come up with a feature list, and the architect can help prioritize work and design the system, as time goes on, you can release software when pieces are ready.

Architects should be able to work on a team. Don't trust an architect to go off and work on their own. You can, as one person, do a LOT with software. Startups and indipendent developers can still get a lot done with one developer (So, there are exceptions, when just starting a project, when starting a new business, etc.). If you need >10+ developers, then your software is more complicated than one person can handle, so *EVERYONE*, including architects, are on a team.

You may be tempted to have non-developing architects. These types can provide vision, and broad direction. They should never be making time estimates, cost estimates, or promises. A team can provide the information needed for those things.

## 2. Make the pay right

"Oooh, now I get it, a developer writes a whole set of blogs about how to make a good dev team, and one of their points is they need to be *paid well*? Huh, sounds like you are just trying to get a raise."

Okay, I get it, I see this argument, but it's still true, it's just true. You have to pay *anyone* well for good work. Yes, there are easy development jobs, in theory. I think that we've overcompensated when it comes to cheap developers and cheap code. Most applications can be built with less developer staff, but each developer being better. I can't tell you how often I've seen (And as a consultant, you can trust me on this) massive teams with so many developers not able to do any real work. Not able to really participate appropriately. Just dragging their feet along, because they don't really know what they are doing. The good developers volunteer to help others, and eventually get fed up and leave for better jobs. Software **IS** hard, and if you aren't prepared, it's very frustrating dragging someone through it.

What is competitive pay? Any number I give will inevitably go out of date soon. Frankly, if you are in a large org, you probably already know what competitive pay means. You probably already have tons of statistics about pay and who applies for these high paying jobs. Pay well, in the 90 percentile, and you will get better developers applying.

Of course, if we all actually pay well, that scale changes, but you get the picture. There's a feedback loop, if you pay low, you get less skill, if you pay well, you get more skill.

## 3. Okay, but what else?

What "rules" could you possibly apply to developers, architects, and those teams? Not many.

But principles are great.

Principles aren't rules, but they are guidelines. They are ways to think, prioritize, scale effort.

For example, this is a security *rule*: "Your passwords should be 8-32 characters, contain upper case, lowercase, letters, and symbols"

This is a principal: "Passwords should have a high degree of entropy, so that brute force guessing should be reasonably useless"

Interesting huh? What does the principal have over the rule? It *changes* as time changes. What is high entropy? Well, how fast are computers? How fast can you brute force? Who is attacking?

You can change principles over time if they are outdated. Some forms of user management don't even include usernames and passwords anymore. Principles chance much slower than rules though.

So, what principles are good for developers? In theory, I think it's important to just *have* principles. But here are some good starters:

1. Keep learning
1. Test code thoroughly
1. Write code for your future self, and for future others
1. Produce constant improvements
1. Always do your best work

Principles can be super personal, the right principles are different for everyone, but in theory, I would ensure that developers and architects just *think* that way. Less rules, more guidelines.

# Your dev team is only as good as your teams culture

There is no such thing as "good" culture, but there certainly is "bad" culture. The people on your team should *want* to be there, not *have* to be there. Your team members should get to know each other, they should like working with each other, and yes, when controversy happens, which it will at work, you need to deal with it and not sweep it under the rug.

Good teams care about working together and learning together. Autonomy is important, the team needs to have the freedom to try things, experiment.

You can't force people to be happy at work, you need to set up work to be a happy place. If your devs are happy, treated equally, have autonomy, and are self-motivated to be great developers, you will make incremental progress over time.


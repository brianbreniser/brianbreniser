---
title: Threat modeling your life
date: 2020-09-08
tags: ["software", "security", "random thoughts"]
draft: true
---

Who wants to get into your data? Why? How did they find you? How did they decide to attack you? What can they get? Let's examine:

Mass attack Matt: Peruses public breeches, script kiddie, has no money.
- Use unique passwords and emails
Scammer Sam: Finds names/emails/phone numbers for general, but individual, attacks
- Watch out for phone calls/emails/sites that are phishy
Business Ben: He makes money buying breech data and exploiting it. Doesn't care about you. Builds script kiddie tools
- Same as script kiddie tools, but even more so
Personal Vendetta Pam: She knows you, wants to hurt you
- Don't fuck with shit heads. If they actually are good then ensure high quality controls are on
'Merica Man: It's the gov't, they can't be stopped
- You're fucked

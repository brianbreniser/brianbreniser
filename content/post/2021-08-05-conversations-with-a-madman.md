---
title: A conversation with a mad man
date: 2021-08-05
tags: ["random", "funny", "self-inflicted", "software"]
---

## === A long time ago ===

Developer: We deliver our application every 3 years.

User: Well, that's a long time to wait for a release, and sometimes it's broken when it gets here. Is there a better way?

Developer: How about we patch every few months.

User: Deal.

# === Time passes ===

User: Man I wish I could get software a bit faster.

Developer: We can deliver twice a year.

User: Every 6 months isn't bad. If you still patch bugs in between that works for us.

# === Time passes ===

User: Even 6 months is getting kind of slow. Got anything better?

Developer: We can be pretty consistent pushing 12 times a year.

User: Once a month is awesome! We can get new features and bug fixes all at the same time, thanks!

# === Time passes ===

Developer: We can deploy 26 times a year now.

User: Oh wow... That's a lot. Well, thank you.

# === Time passes ===

User: Hey, getting new apps every 2 weeks is kind of a pain. Plus like 3 times a year it completely breaks. Maybe slow down and make sure every feature works before releasing it, okay?

Developer: We now deliver 365 times a year.

User: WOAH WOAH WOAH! We don't need that, I don't even WANT to download your app once a day.

Developer: No problem, it's now a web app, so you have to download the entire thing EVERY TIME YOU USE IT. It can't possibly be easier!

User: Wait, this thing is slow! It's also broken like 3 times a week. Can we go back to high quality development and new features every month again?

# === Like, literally the next day ===

Developer: We now deliver every 5 minutes, 100,000 times a year

User: WHAAAT! NO NO NO NO, stooooooop! I want high quality code, not junk thrown at me every 5 minutes! STAAAWWWWP!

Developer: Good news. I can deploy on every. Single. Commit.

User: OH god please no. I'll never have a stable app to work in ever again!

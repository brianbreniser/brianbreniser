---
title: Full time pair programming rocks your socks!
date: 2021-08-06
tags: ["teamwork", "theory", "opinion", "soft science", "social"]
---

# Don't just listen to me

Pair programming is a big topic, a lot has been written about it, and I don't think I'm better at arguing for it than anyone else. However, as a consultant, I've worked on many teams in many situations, and recently, I've been working on a team that pair programs full time. In this article I'd like to share my experiences, and my reasoning for why I prefer full time pair-programming.

# TLDR

I like to write longer-form articles, so I have a lot of words to write later, but for now, this is a decent TLDR:

If you are doing code reviews, you should instead be doing pair programming, as code reviews are less effective or will take longer for the same effectiveness compared to pair programming. This works up to a limit of team size and project complexity.

Pair programming is really rough when A strong programmer pairs with a weak programmer, or a loud personality pairs with a quiet personality. Mitigating this is essential to be successful. Pair programming is also a collaborative tool, so make sure the team is on board before trying it.

# The real reason to pair program

Okay, so my reasoning about why every team of small to medium size should pair program comes from my experience when comparing pair programming to code reviews. Code reviews are really hard, they are a waste of time at worst, and time consuming if you do them correctly. It is worth it to do code reviews correctly, but they will (nearly) always be inferior to pair programming.

I'll only compare successful pair programming in this section, we'll talk about a failure to pair program later.

## When code reviews suck, they are a waste of time

A bad code review might mean the reviewer doesn't really understand what they are reading, or at least don't understand the intention of the original author. This kind of code reviews typically nitpick unimportant details because the reviewer will feel obligated to write something, the original author may feel attacked, no one will win.

In this case, code reviews are a waste of time. You will actually get more effectiveness out of not doing good reviews at all. Unfortunately, not doing good reviews is worse than doing good code reviews, and both are worse than doing good pair programming.

Bad good reviews are bad, but that's a low bar. Let's compare to better code reviews.

## When code reviews are fine

So let's talk about code reviews that are pretty good. By this I mean that everybody accepts that code reviews are good, and everybody genuinely tries hard to review code in an honest way. There's still a problem with this, having a second developer understand what the initial developer intended to do is very time consuming. The decisions that the initial developer made are out of context for the second developer, who now has to play catch up to what they really meant in the first place.

In this case, the second developer is likely to miss a few things in the code review. It's very difficult, even when you're trying very hard, to thoroughly read every changed line of code and understand it well. For example, reformatting changes or white space changes throw a lot of noise into code reviews, making it harder to find important sections of changes. This can be mitigated by having smaller code reviews. Though of course, that would mean there are more code reviews, just that each one is smaller.

My argument is this, pair programming is a more thorough second check by a second developer, because they understand the context the original developer was in. They know when there are white space changes, and can ignore that on the fly. A pair of programmers are going to do a better job of focusing on the parts that matter and coming to a collective understanding at the time the code is written in the first place.

Okay we've covered the obvious case, and we've even covered the average case, now let's cover when code reviews are actually really great and effective.

## When code reviews are great

Really effective code reviews mean that the developer was very clear regarding their changes. It also probably means that the team has good standards for dealing with code style, whitespace, etc., separately. Things like this can be dealt with by a linter that runs on every commit, for example.

We also assume that the reviewers take the time that is necessary to understand the code the way it was intended, and provide meaningful and unambiguous feedback to the developer. The back and forth from developer to reviewer is professional, and respectful.

So, let's face it, that's going to take a lot of time. A fast code review isn't good, a good code review isn't thorough, but a thorough code review just takes too much time. The original developer has to write the code, the reviewer has to read it and understand the context, which could take as long if not longer than the writing it in the first place. Then, the original developer has to make changes that the reviewer suggested, and the reviewer has to review those changes again. This cycle can go on a few times, delaying code releases.

Pair programming is just faster to do really thoroughly when compared to a thorough code review. Pair programming means that 2 developers work on the code at once, understand it at once, bounce ideas back and forth, and refactor at once. In the end, if you want 2 eyes on the code, pair programming is faster.

This is a bit of a surprising result. I've read that pair programming is faster than 2 developers working independently, we'll look at exceptions in a minute, but now I think a result like that can be achieve when taking into account code reviews. Pairing can actually go faster if the two developers can catch coding blockers faster than one programmer too.

In the end, I think pair programming produces higher quality code. There is always someone there to catch simple errors, which we all make no matter our experience level. It's hard to take shortcuts, because you are always being watched by someone, and code reviews can miss some of those shortcuts. I think that the code quality aspect alone means pair programming should be at the top of a team's list to try.

# When pair programming fails

Nothing is perfect, and that's true about pair programming too. These 3 situations cause the most problems (In no particular order) when it comes to pair programming:

## Loud personalities, or a big skill gap.

This will lead to effectively a single programmer doing all of the work. No benefits if pair programming will be realized, and the weaker developer, or the more quiet developer, will feel like they are wasting their time.

### Mitigation

Though this will happen from time to time, it's important to work though ti. It is possible to coach a weaker developer (Easier said than done, but still true), and given enough time they can work at the same level. It may be best to pair that developer with someone only just above their skill level, so they can learn without going over their heads. As for coaching loud personalities, if a developer is willing to work on team dynamics, they can set aside their comments for when the time is right.

## Time zones

Okay, so I program 3 hours apart from my team, that's *not* what I'm talking about. It's pretty convenient for a work-from-home type to adjust a few hours to meet a team. The problem comes up when you are many hours apart, think 8+. Pair programming like this is basically impossible. It's just not fair to make someone program in the middle of their evening or night, when the other part of the pair is working during regular hours. I've seen teams try to switch who works evenings, and it's no fun. It's always a losing situation for the team working nights.

The only exception to this is someone who *wants* to work evenings/nights. It's important that we take into account an individuals choice when hiring outside your timezone. For me, getting up 3 hours early is worth getting off 3 hours early, for others, the opposite might be true.

### Mitigation

It is possible to have 2 teams working on the same project that pair within teams, but not between teams. Though, this is a tough situation for any team, doing code reviews or pair programming. If there is any overlap in working hours (Teams less than 8 hours apart), they can spend part of the day pair programming between teams, and the rest pair programming in their own respective teams.

## Poor developers

I'm all for giving everyone a chance, but a poor developer will damage any team and any system. I mentioned in the first point that a weak developer can be coached. This is true and should be tried! I'm talking about the dev that just doesn't care, maybe they don't like the team, or don't have motivation to get better. These are toxic members in any team, and are completely incompatible with pair programming (Or code reviews for that matter).

### Mitigation

Coaching, and if coaching isn't working, it's probably time to look at re-assigning the developer.

Okay about that last point, what about interns? Jr. Developers? Can they join? Yes! In a pairing situation where you know one person is more novice, but eager to learn, the stronger programmer can act as a coach! As long as they don't do that 100% of the time. It is exhausting spending your whole day coaching, but totally possible for part of a day.

The intern or Jr. should be able to keep up in a few weeks, being a productive member of a pair themselves. Of course, there will always be people who can't keep up, and if a team does regular check-ins, their people manager can help find resources to help them out.

# When should you NOT pair program?

Ah, now we get to an interesting section. The section needed because programmers are extremely pedantic (Hey, that's why most of us are here! Programming requires a bit of pedant!). Pair programming is NOT the best style of programming for every task. Let's see where:

## No one wants too

This first point can be true for a lot of things, if a team doesn't want to do something, forcing it is not a good idea. Though, there are some things, security comes to mind, that can be forced, but this is not one of those things. Pair programming is really nice, but if people aren't on board, it will fail. I recommend trying pair programming within a pre-set trial period, say a month or so. That way, people won't feel trapped into doing something forever that they don't want to do.

## Tasks that are too easy

Ever come across a story so obvious that anyone on the team can work on it and be basically 95%+ sure that it will be fine? Don't pair on these items, just work on them, submit a PR, and the review will just as easy/obvious as writing the code.

Unless you like company when pairing, 2 people could work on 2 things at once. In a work from home setting (Common nowadays!) it can be nice just to have some digital company while working independently. 

## Tasks that are too hard

Every time I need to sit down and absorb some really complicated documentation, or lack of documentation, or some obscure library, it's much easier to go at that alone. For some people, just putting on some non-vocals music and reading up is much more effective at absorbing complicated code/documentation that doing that while being in a conversation with someone. In the end, you can always write up or summarize what you learned for others.

As well, high level architecture diagrams can often be built alone, and feedback given from the team at a later time. This isn't code per-se, but it can be a major part of a feature before code is written. You can pair on these kinds of items as well, but that will be up to the team members to decide.

## Documentation

Don't you just love writing documentation? I sure don't. This is another thing that can be done alone, preferable with some non-vocals music. 

# Hints and Recommendations

Time for the section where I list out some really strong recommendations based on my time pair programming for about 6 months full time.

## Take regular turns

Especially when trying pair programming at first, take very strict turns. The team I'm on right now has a program that overlays over the whole window after a 10 minute timer. That's right, only 10 minutes per person! You can probably get away with 15 minutes or so, and possibly as little as 8, but less than 8 minutes and there is no flow you can get into, and more than 15 will feel like burnout to any individual.

This can depend on the task at hand, and advanced pair programming members may not use a strict timer. It's possible to hand off pairing at good stopping conditions while programming, but I would not recommend any one person being on the keyboard for more than 20 minutes.

## Take breaks

Most people can only focus hard for about 25 minutes, and when taking turns you can get away with up to an hour. Our team has strict 10 minute breaks every hour, but I would even recommend 5 minute breaks every 25 minutes. In such a case, you and your pair can do 12.5 minute rounds.

## Encourage others

Pair programming is hard! It's a skill by itself in addition to programming skills! When you try it, you may feel discouraged at times, keep in mind that you are not alone. It's so easy to freeze solid and feel severe judgement when you know someone is watching you program. It's important to let others know that they are doing well, ask when they are stuck, and keep a positive attitude when someone doesn't know what to do.

## Have dedicated non-pairing time

I would not recommend paring for a solid day, take an hour at the beginning or end of the day to do non-programming tasks, and take a full hour lunch for a break. If your team needs, don't feel bad about taking 2 hours to do non-pairing work per day. Especially if you are just starting out. I would also recommend half a day to a full day a week where the team does not pair program. During this time you can fit in all your weekly/sprintly meetings. If allowed by your company, you can work on side projects, or refactoring code that you can submit in a pull request (Simple refactors, not major refactors!).

# Okay, that's it now

Pair programming is hard, but I think it's worth practicing. Pairing with test driven development is a good combo, but if you team does neither, introduce one at a time. I recommend pairing first, since TDD requires a major shift in thinking, which is easier to do with someone else helping out.

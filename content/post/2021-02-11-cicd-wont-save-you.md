---
title: CICD won't save you from bad design
date: 2021-02-12
tags: ["software", "engineering", "architecture"]
---

# The series

1. [Part 0: The intro]({{< ref "2020-03-24-the-real-challenges-of-software-engineering-series.md" >}})
1. [Part 1: The categories and common terminology I'll use]({{< ref "2020-03-24-the-categories-of-software-development.md" >}})
1. [Part 2: Marketing and conversation is focused on the wrong areas]({{< ref "2020-03-24-organization-spend-time-in-the-wrong-areas.md" >}})
1. [Part 3: Only good design will save you from these challenges]({{< ref "2020-03-24-only-good-design-saves-you.md" >}})
1. [Part 4: DevOps won't save you from your challenges]({{< ref "2021-02-10-devops-wont-save-you.md" >}})
1. [Part 5: CICD won't save you from your challenges (This part)]({{< ref "2021-02-11-cicd-wont-save-you.md" >}})
1. [Part 6: The final answer to good software devlopment]({{< ref "2022-01-26-the-final-answer-to-good-software-development.md" >}})

(Hey, this article took a year to release! Actually, I wrote it about a year ago, and never got around to proof-reading it until now. Lame, I know. That's just how it goes sometimes. I hope you still enjoy it!)

Check out the [software development categories]({{< ref "2020-03-24-the-categories-of-software-development.md" >}}) before reading this, It's a short read and will make a lot more sense.

# But no really, I have the solution!!!

If you've read up until now you just heard that DevOps won't save you from bad code design.

Some of you are shouting *even louder* now, because of course DevOps isn't the answer, the answer is *CICD*! You see, CICD is different because CICD is...

No... still no, CICD isn't the answer.

# How is CICD different from DevOps?

Some of you might be wondering how DevOps and CICD are different. They overlap in organizations quite a bit. A DevOps developer (*/me Shudders when writing that*) often writes CICD code. They spend a lot of time in pipelines and the domain specific languages (*/me Shudders thinking about Groovy*). Unfortunately this is another problem in software development, overlapping too many things into one.

# Really quick, what is CICD?

Continuous Integration, Continuous Development.

TLDR: CICD is the philosophy that you should push your code to test environments, test it, then push to the production environment, all automatically, without human intervention (Other than kicking off the process).

It's essentially just automation of your build system. Write code, push it, and the CICD pipeline will automatically build, test, scan, and deploy your app to a testing location. Then you can test it, then deploy to another location for new tests, etc. until to are live. In theory, the more automated, the better you are at CICD. There's more than one right way to do CICD, but normally, this is a place you can rely on tools to help you, because rolling your own is a real pain. For small projects, rolling your own is fine, or even using a non-customized solution out of the box can work great! For bigger projects, you will need custom build code, but you can rely on CICD tools to run them.

# Why isn't CICD the answer?

So why do people *think* it's the answer? Because it involves a lot of testing. Evey push on a branch will result in building, testing, and deploying, which does cover a *lot* of ground. It helps you find issues you can't find when just writing code locally.

But CICD can help you *find* issues. It can't help you *solve* the issues. In fact, doing CICD in a big project well, requires writing a lot more code. Code that can break, code that needs to be verified. If you are having issues writing your app code, then at best, CICD code might not be any better, at worst it will get in the way.

There is a silver lining with CICD. Though it can't help you solve problems, I did say it helps you *find* problems, which is a start! If you do thorough testing before deployment, however that works for you, you will find issues. Finding issues is the start of solving issues. I highly recommend CICD to *help* with good development. By itself though, it's a tool, and tools can be used poorly.

If you have poor development teams, with poor architects, your app will still be hard to write, you will still spend all your time in the wrong paces.

When it comes down to solving those issues, CICD can't help.

In the next part (Currently not posted). I will discuss the things that *actually* work. I've covered them at a high level already. Have good developers, and good architects... But that's not a satisfying answer. What "IS" a good developer or architect? Can I train one? How much should I spend? The next section will help cover this. By the way, I won't *really* give you an answer, I will open a door. You will need to walk through it.

---
title: I'm not the goto guy anymore
date: 2024-04-28
tags: ["career", "random thoughts"]
draft: true
---

# I'm not the goto guy anymore

Coding is a young mans game... Kinda... We'll come back to that later. You're a big hot shot when you've come out of college after a successful set of internships, you talk the talk AND walk the walk, and you are willing to dive straight into any work put in front of you. New guys are a companies favorite.

I've never been young in this field, actually...

I Have 3 undergrad degrees, and long story short, I didn't get my computer science degree until I was over 30 years old. That means when I entered the field as a junior I was bald, lower on energy than my peers, ready to settle down, and although I was up for adventure, it didn't last long. After a few years I was already hitting my wall. People in my age bracket were architects or moved on to project management or something. They remembered the "good old days" of programming in the... late 2000s... Anyway the point is I was never a young guy, but being a newbie is still a heavily... how do you say... advantaged position to have.

You see, young people have a few very specifc, very particular set of features that corporate america REALLY likes. These aren't necessarily a secret, so let's just list them:

1. Young people have energy
2. Young people have drive
3. Young people have need to prove themselves
4. Young people are unnattached
5. Young people are moldable

You get the picture. Young people have something to prove, and corporate amaerica wants wants that energy. But there is another thing that young people will forever have on old people:

# Young people are cheap

I currently (Keep in mind this is estimated with benefits including some tax advantaged things) make roughly 185K/yr. I started out at 75k with a 10k starting bonus. Some of my friends started out at 60k, and one at 50k.

For the price of me right now, you could get roughtly 3 newbies. I am someone who prefers not to travel becuase I have a family, someone who doesn't have much time outside core working hours becuase I have 2 young kids, someone who knows his skills and doesn't care what you think about them, someone who cares more about his retirement than his current project that is (probably) just like the last 10 projects. Software reinventing the wheel for the umpteenth time.

Old people have history, and corporate america has beat their sense of wonder out of them.

But to address that "kinda" line from before...

# Is coding a young mans game?

I said I'd come back to this. So what? Is this true?

So, the older you get, the better at coding you get, for sure. You learn a lot of lessons over the years. You know where to trim fat, you know where you can focus your work. You know many languages and maybe if the project calls for a new one you can spin up very quickly becuase, hey, you've done it so many times before. The reality is that I'm a better coder than I was 8 years ago when I started. I know this. But am I 3 times better? Maybe, I suppose you "could" say that if you take into account not just being faster, but also avoiding more pitfalls, doing the right thing first, not bikeshedding, then yeah 3 times better, for sure, maybe more.

But here's some facts for you folks. Coding ain't that hard.

I mean, again, I admit I'm probably like 4-5x better than I used to be, but that kinda doesn't matter, because *most* code doesn't required someone 5x better than a newbie. Most code requires a decent amount of reading getting started guides and looking stuff up on stack overflow, and nowadays, copilot will take you a long way.

Most code isn't hard enough to throw a high quality developer at. Web apps are like 90% of development now, and well, code schools were cranking out those devs by the hundreds awhile back. Their reputation was pretty bad so that kinda doesn't work anymore. Still though, a code schooler who did extracaricular work is probably just fine for *most* tasks.

Which is why instead of having like 8 awesome devs on a team, most companies put 1 experienced guy on it, and maybe 1-2 people with a few years under their belt, and like 5 newbies. Because there probably is 5 peoples worth of boring grunt work to do for every 8 person-hours of work to do total, and only 1 guys really needs to have a full handle on the project. Because heck, most of the time you're probably building some simple project that the experienced guy could knock out on their own, so long as their users could use a command line. But, because users are all devs act like normies who only know how to use webapps (Even when they are other developers, who even reads the man page anymore???), you're forced to do a ton of grunt work making a pretty website with lots of animations.

Okay, if you aren't building for other developers then I guess that means you gotta put in the work to make apps usable. But even then, 99% of that code isn't seen by humans, even for customer facing apps, 99% of code is just code for other coders. Libraries, dockerfiles, servers, JSON, YAML, on and on and on. Still, most of the time I could crank out a decent cmd for something if my users were smart enough to read a man page, or a README file, or whatever. But instead, we build webapps and prettified documents for our peers, becuase they never learned linux, and refuse too...

# Well, that was kinda a rant

Sorry, I guess it was, wasn't it?

# But you like coding, right?

I LOVE coding, I love the rush of putting together someting and seeing it work, I like staying up late at night with only the glow of the screen, chillstep or dubstep or lo-fi hip hop playing in my earbones. I still get a kick out of re-building my linux os, customizing it, borders, colors, eye candy. I've written neat useful software for myself, I get shit done now. I can do anything, and it's not even a big deal. I love seeing a problem, doing some kagi searches, finding something on Reddit (Or lemmy preferrably now) that no mere mortal would know how to find, grabbing a new cmd, passing my file to it's input, flagging it to output what I need, using chatgpt to explain the command (And yes, of course the man page, but AI is actually pretty neat when it comes to faster documentation), and dumping the result. Never needing to leave my terminal and a search bar.

There's just a rush to knowing how computers work. They are such an enigma to normal folk. Most people have no idea how I do my career and hobby, no one knows how to do computers but those of us who've put in the time to learn what a command line is. What we've done is nearly impossible for regular people to understand. But I can learn their things...

I've learned how to do woodworking, it's amazing craftmanship, but it aint hard. Time consuming, sure, but with the right tools and some practice, you get the hang of it.

I don't mess with cars much, but I've done that before. Hard again, time consuming, expensive as hell, but still tactile. You look up a manual, buy the parts, have a ratchet set, and boom, you can do car work. Just not in the age of computer cars, no one can fix those.

I've poored conrete, used an excavator, designed and installed custom lawn watering systems, built over a hundred feet of retaining wall over 3 feet tall, I've built an 11x11x11 shed from scratch, on the side of a hill, I've rebuilt half of another shed, built 2 decks and helped build 2 others. I've built big strong shelves you can climb on, I built custom closets for all 5 bedrooms of our house, built custom pantry shelves on drawer slides, many steps and short stairs for our cat who isn't so able bodied anymore. I know how to calculate board-feet. I know what a mortise and tendon is.

I know how to buy stocks, I built my own retirement portfolio with cheap broad-based index funds, I wrote an investment calculator to help plan out my retirement. I know what stocks, bonds, and t-bills are.

I also know how crypto works, large language models, and all the other stupid tech-of-the-day things.

I also lift weights, and my powerlifting record is over 1,000 lb of total weight over squat, bench press, and deadlift. I do overhead press too, and rows, and some minor bodybuilding.

And yet, no other person I know personally knows how to customize a sway-wm setup with custom colors and key bindings.

Knowing about computers is just... Magical. I will never regret learning about computers.

# But I can complain more anyway

After all this... I can no longer stay up late playing with my computer building neat things, I have kids that will wake me up at 6am no matter what time I go to bed. I have sleep apnea and vertigo, so going to bed is a nuscence and it takes a lot of work to sleep well. My hands are all tendonitisted and my writs are carpal tunneled and my fingers get numb after typing a lot. I've got the most ergonomic damn setup you can imagine and I still need physical therapy to stop pinching my back nerves so that I can feel my fingers while typing. I'm tired, I'm pissed off politically, I don't care to play games.

# I'm just not a good corporate stoog anymore.

In the end, I'm just not your corporate go-to guy anymore. I don't care about your companies goal of stealing more money from it's workers, ripping off your customers ("legally"), re-inventing yet another thing becuase the software we had 10 years ago looks "icky"... Get over yourselves. I'm an old crumungeon. Get off my lawn.

There are projects that excite me though, but those require some real time commitments. You gotta have a 16 hour-a-day mentality for some of those projects, and I couldn't do that even if I wanted too now.

For anyting I want to do, I don't have the time, for anything I can do, it's dumb and boring and I've done it X times before.

Plus, I'm too expensive for grunt work, and my family relies on that money, I can't just take a pay cut becuase I can find a more fun job if I decided I wanted to trade that off.

I'm just not the corporate go-to guy anymore.

And I guess I'm okay with that

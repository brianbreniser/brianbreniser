---
title: Hello blog
subtitle: First post!
date: 2019-10-20
tags: ["freethought"]
---

Hello friend.

<!--more-->

This is my new blog, website, landing page, and freethought space. Here you will (Eventually) be able to find information about me such as my resume, current job, thoughts I have, and maybe even some tech content.

For now, this is my first post (An old post from an old blog will be ported over as well). Thanks for checking out my page! This entire site is open source, and can be found in the various links to it's gitlab source code. Feel free to copy the site design.


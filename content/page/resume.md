---
title: Brian Breniser - Resume
comments: false
---

# My experience in short descriptions

#### Software development

I'm a Senior software developer and I love building useful software for regular people and technical tools for my fellow developers. I've molded myself to many different roles over the years, and I continually practice web development, CLI, WASM, and GUI development. Most of my development experience has been on the backend with web development, DevOps, and AI.

#### Ops and DevOps

I've worked in DevOps roles on many teams. I've maintained CI/CD pipelines for dozens of developers in Bamboo, Jenkins, and Openshift pipelines. I've maintained Kubernetes & Openshift deployments manually, and with Helm charts. I've also taught security from a developer's perspective, explaining and defending multiple layers of scanning in a developer's workflow and container management, and bug reduction from high quality testing & deployment perspectives.

#### AI

My current side projects involve Large Language Models. I'm building out tooling to set up some projects for AIs as a service, APIs to utilize various AIs, and regurn augmented generation (RAG) utilizing vector databases.

#### Back end

I've built many backends with multiple languages and frameworks. My most heavily used languages are Java/Kotlin with Spring. My favorite backend languages are Go and Rust (Yes! I like both of them!), in my opinion, they offer better performance and ease of use than JVM languages. Databases are no stranger to me, but Postgresql is my most familiar.

#### Front end

I'm no graphic artist, but I can implement a design in HTML/CSS. My personal designs are generally more practical, coming from an engineers perspective, but I'm learning how to design graphical interfaces for regular people. I've recently worked on dynamically scaling UIs that work across 30-inch monitors and mobile phones. My tech stack experience starts with the usual, JavaScript/TypeScript with Angular/React, and extends to the unusual, building cross-platform native/WASM apps with Rust and Yew.

#### Teacher, Documentor

My time in software development and consulting isn't all code, all day. I've had to develop soft skill as well. I've produced videos teaching basic security topics from OWASP, and teaching people how to break into the Juice Shop (https://github.com/juice-shop/juice-shop). I've received very postive feedback on the written and video tutorials I've done.

# Education

<br>

#### Bachelor of Science

##### Portland State University, Portland Oregon

Computer Science, **obtained** Summer 2016

<br>

#### Bachelor of Science

##### Portland State University, Portland Oregon

Psychology & General Science, **obtained** Summer 2010

# Work Experience

#### Red Hat

- Sr. consultant
- Jan 2019 - **Now**
- Remote & Travel

#### Description

- As a Sr. AppDev Consultant, I use Kubernetes/OpenShift to assist in implementing software for our customers, and development tools for developers.
- Customers require a wide variety of work to be done, it's my job to be able to mold myself to the role and get what needs to get done.
- The biggest project I've worked on in a developer capacity was an internal full stack application in Java/Kotlin with SpringBoot, and JavaScript/TypeScript with Angular/React (Migrating from Java->Kotlin, and Angular->React while I was there). We built a generic automated project-creation app that generated a project using a number of language options, with frameworks and library options to choose from. The user could also choose to generate deployment files, and choose to allow the files to deploy the hello world generated app to a live test namespace. All files were put into git automtically with the user as the owner of the space.
- I've traveled around the USA working in customer’s offices, as well as worked fully remote in my home office.

#### Cayuse LLC

- Software Engineer I
- July 2017 - Jan 2019
- Portland, Oregon

#### Description

- For the first 3 months I worked on  maintenance of  a legacy product written in coldfusion, focusing on bug fixing and adding minor features.
- For the rest of my time I worked on the next version of the core application. It was written in Java/Spring and ran;w
- on AWS Lambdas. We migrated our functionality to REST services, and wrote our front end in classic BackboneJs.
- I maintained their classic microservices implemented in docker written in NodeJS and Java with SpringBoot. I implemented caching using Google Guava Cache and other performance enhancements to lower microservice response times.

#### CDK Global

- Software Engineer I
- July 2016 - July 2017
- Portland, Oregon

#### Description

- My primary goal at CDK was to build an electronic payments application for customers.
- I designed distributed services in Java with SpringBoot for the new infrastructure, while maintaining a legacy product on a monolithic infrastructure leveraging CentOS and PHP.
- I played the role of scrum master, as well as developer, on this team to facilitate good agile practices and lead by example through ceremonies and quality code.

# Technology summary

#### Things I love

- Linux
- Golang
- Rust
- Kubernetes
- Helm
- Python
- Podman
- Postgresql

#### Things I like

- OpenShift
- Tekton/Openshift pipelines
- MacOS
- Kotlin
- TypeScript

#### Methodologies I have experience using

- TDD
- Pair programming
- Mob programming
- Agile
- Kanban

#### Frameworks used

- React
- Yew (Rust react-like framework)
- SpringBoot
- Quarkus
- Elm
- seed-rs (Rust elm-like framework)
- BackboneJs
- Angular
- Django
- And many, *many* more

#### Other Technologies I've experienced worth mentioning

- HTML, CSS, webassembly, and other web technologies
- MongoDB, DynamoDB, and other alternative databases

#### Dependency managers I'm familiar with

- Cargo
- Go
- Gradle
- Maven
- Helm
- npm
- pip

#### Build systems I have extensive experience with

- Cargo
- Go
- Gradle
- Maven
- Helm
- Makefiles
- Bamboo
- Jenkins
- Tekton
- Shell scripts

<br>

# Other information about myself

<br>

Connect with me online:

- [https://github.com/brianbreniser](https://github.com/brianbreniser)
- [https://gitlab.com/brianbreniser](https://gitlab.com/brianbreniser)
- [https://linkedin.com/in/brianbreniser](https://linkedin.com/in/brianbreniser)
- [https://twitter.com/BrianBreniser](https://twitter.com/BrianBreniser)

#### Conferences I co-founded

I co-founded the CatBarcamp at Portland State University, a tech un-conference which brings in around 100 participants from Portland every year. The website can be found at [https://barcamp.cat.pdx.edu/](https://barcamp.cat.pdx.edu/) and features DevOps talks, software development talks, and other talks that other Portlanders are interested in.

#### Conferences I've attended

- OSCON
- Red Hat Tech Exchange
- Mozilla Rust monthly talks (No longer in session)
- Hackathons & coding events around Portland
- Leadership meetups around Portland

#### Conferences I've spoken in

- Monthly rust meetup (https://www.meetup.com/PDXRust/?_cookie-check=ST8aVAl0_xo18lae)
- Seattle GNU/Linux conference (http://seagl.org/) regarding my Capstone project where I lead a team of students building a simplified rocket guidance system written in Rust for the Portland State Aerospace Society  (http://psas.pdx.edu/).

References are available upon request


---
title: The real challenges of software engineering series
date: 2020-01-14
tags: ["software", "engineering", "architecture"]
---

# What's this all about?

I'm kicking off a 6 part series about the challenge of software engineering, how our industry is focused on the wrong areas to meet those challenges, and what you can do to make your software development life as easy as possible.

# The series


1. [Part 0: The intro (This part)]({{< ref "2020-03-24-the-real-challenges-of-software-engineering-series.md" >}})
1. [Part 1: The categories and common terminology I'll use]({{< ref "2020-03-24-the-categories-of-software-development.md" >}})
1. [Part 2: Marketing and conversation is focused on the wrong areas]({{< ref "2020-03-24-organization-spend-time-in-the-wrong-areas.md" >}})
1. [Part 3: Only good design will save you from these challenges]({{< ref "2020-03-24-only-good-design-saves-you.md" >}})
1. [Part 4: DevOps won't save you from your challenges]({{< ref "2021-02-10-devops-wont-save-you.md" >}})
1. [Part 5: CICD won't save you from your challenges]({{< ref "2021-02-11-cicd-wont-save-you.md" >}})
1. [Part 6: The final answer to good software devlopment]({{< ref "2022-01-26-the-final-answer-to-good-software-development.md" >}})

# Okay, but what is this really about?

A lot has been written about the challenges developers face in software engineering...

a LOT.

But hey, why not one more?

I'm not the most seasoned developer, but I've been working full time for over 3 years now. I've moved from active software development to a consulting role in software development and DevOps. When I look back on what I've worked on, companies I've consulted with, blogs I've read, and as I look forward to the near future of software development, I see a some common challenges.

These challenges have nothing to do with waterfall, agile, CICD, DevOps, or any tech stack. They are about the actual act of writing software, regardless of methodology or strategy. These challenges arise if you are working in a world class organization on million+ lines of code projects, or weekend personal projects.

Also, take some of what I say with a grain of salt. I take a reductionist approach to the ways in which people write software. Being a reductionist never wins any awards, but it does allow us to categorize things. In the end, this makes sense to me, but might not make sense to you, and that is okay.

# The full TLDR

If you don't have time for 6 parts, that is fine. There is no secret I'm trying to hide, and nothing I'm selling you. In my opinion good information being shared is more important that someone reading 6 blog posts in full.

That being said, there is a lot of context in the next 6 parts that helps explain the TLDR. Keep that in mind if this sounds counter-intuitive.

In general: Individuals and companies buy/sell/invest in the wrong areas of software development, with a misguided idea that a certain product will make their engineers better. In reality, these products focus on making the beginning of software development faster, but the difficult things about software development is maintenance and architecture, for which no product is effective at solving. The only solution to time consuming software, software that is difficult to change, and software that needs to last a long time, is to hire good architects that can design a system that is easy to maintain.

If you need more context please read on.

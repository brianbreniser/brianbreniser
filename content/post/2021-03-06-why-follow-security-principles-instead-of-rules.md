---
title: Why you should follow security principles instead of rules
date: 2021-03-06
tags: ["software", "security", "random thoughts"]
---

Here are some security principles:

1. Least privilege principle
1. Least trust principle
1. Simple solution principle
1. Auditing principle
1. Secure failure principle
1. Don’t reinvent cryptography principle
1. Find weak point, fix it, repeat

Principles copied from [This cool talk](https://www.youtube.com/watch?v=YbjoaMN67Hw&t=3746s)

Why should we follow principles? What do we gain compared to just following some simple rules? I'd like to give an example. Let's use the principle of "least trust". Least trust means do the least amount of trusting of anything that you don't directly control. Though, obviously you must trust **something** at least **sometimes**. The pedants in the audience would say that literally trusting nothing would lead you down conspiratorial thinking and cause you to live in a cave, hunt and gather for your own food, and fight away anyone who comes near.

# Lesson 1: Principles have nuance

That's our first lesson about why principles are better than rules. Principles have nuance, you don't follow principles to the letter, 100% of the time. You are allowed to trust things, when appropriate. It's called the "least trust" principle, not the "no trust *rule*". This means that over time, your principles can guide your rules, rules are flexible and can change, but principles last much longer. This leads us to our next conclusion:

# Lesson 2: Principles last longer than rules

Lesson 2 is this, principles last for a long time, even a lifetime, whereas rules go out of date rather quickly. Let's use another example from "least trust": Passwords.

How many of you are in an organization that expires passwords every 30, 60, or 90 days? The reason for this is that for a long time because NIST (The national Institute of Standards and Technology) recommended it. They recommended it because at the time it took hackers about 90 days to crack a password. If you changed your password before that, then they would never get in. Now that computers are much faster, and passwords aren't much better, it takes seconds, or possibly minutes, but never days, to crack the average password. People kept these recommendations around because "just in case" someone was in your system, if you changed your password every so often, that would kick them out.

But this isn't true anymore. NIST not longer recommends expiring passwords. For one, we know that the average password is just too weak to crack, so even 30 days isn't enough. For two, we know that forcing people to re-create their passwords makes them lazy, they have an easy-to-remember password that they will just append to the end the numbers 1, 2, 3, 4, etc. when password reset time comes around. Lastly, we know that attackers that get into a profile will either lock down the account in minutes and spam everyone, or sit on the account slowly harvesting keystrokes, including your next password. Swapping passwords every 30 days does nothing for your security, and it makes bad passwords popular.

# Okay, so principles last longer, what do you recommend for passwords, using your principles?

I'm not a professional security researcher. I practice security as part of my job, so I won't give you ANY rules to live by. But I will use my principles to make general recommendations.

Don't expire passwords, but DO make sure the passwords you accept follow some guidelines:

1. Passwords are NOT in a database of cracked passwords. Keep a list of known password leaks handy, and check all new passwords against it. Where do you fine one? Check security reddits, hacker news, lobsters, showdan blogs, and any other security news sites, for leaks. I can't tell you where to get your list, that's a rule.
1. Check new passwords against a dictionary attack. In short, just make sure that the password isn't just one word, or a word with 1 letter replaced with 1 number.
1. Check new passwords against an entropy checker. This one is harder, but there are various ways of doing this. Calculate the attack space and average computer speed required to guess it (calculation space divided by guessing speed divided by 2 for the average). Then only take passwords that pass a 100 year time-to-guess calculation.

That last check may sound unreasonable. 100 years? Wow that's a lot! But really, that's not even *that* good. We all know that moors law is dead, but even if computers aren't getting faster, they are getting more parallel. There's a limit to this as well though, unless you use GPU's! At any rate, let's use moors law, in 18 months that 100 year password becomes 50 years. In 36 months, that's a 25 year password, in 54 months, only a 12 year password. Though, that password isn't even that long, it's actually a randomized 8 character password with only lowercase and uppercase letters, not even that strong. I personally don't enter any passwords under about a 20 trillion year to crack entropy. I also use a password manager.

Here's a recommendation that is based on **Current** best practices. Use 2 factor authentication with a rotating one time password. Hey, if an attacker can guess your password, they then have to guess your OTP, and if you only give, say, 3 shots at that before locking the account for 30 seconds, then an attacker is significantly slowed down. Next year, I may have a new recommendation, though the 2-factor OTP has been around as a best practice for literally decades, so it's probably pretty solid, which is why I put it here. But it's a rule, not a principle, so it's not in our list of principles.

# More examples!

I could go on, but I want to finish this up. But here are some more recommendations based on PRINCIPLES:

1. For any publicly facing app, make sure to implement users and groups, and ensure that users are given the least privilege until they are added to more groups (Least privilege principle). If new ways of abstracting out privilege levels get popular, consider implementing that.
1. Only implement one user/group system at a time (Simple solution principle). Also ensure that you implement as much of it yourself, and only rely on outside access control lists when you interface with those applications (Simple solution AND least trust) 
1. Log your application's activity, review it (Auditing principle). Also, Audit your own developers actions (Auditing principle). Also, audit your users actions... How much do you need to audit? How much time do you have to do this AND everything else?
1. When you fail in production, don't dump logs to the UI (Secure failure principle). Also use a language that allows you to catch failures, and clean your memory after a failure is found (Secure failure).
1. Use well-known and well-established cryptographic libraries (Don't re-invent cryptography... Telegram, looking at you (*/me glares*))
1. Find weak points and fix them is the only principle on this list that looks like a rule. But in reality, you can implement this many different ways. Agile? Sure. Scrum? Sure. A team running annual reviews? Counts. Just make sure you are making your software better over time.

# Conclusion

Principles create rules, rules can change, principles last a long time. Principles have nuance that allows us, as humans, to make smart decisions based on recommendations, not blindly following the blind.

Lastly, don't trust me, I'm just some guy on the internet.
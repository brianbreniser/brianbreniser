---
title: The final answer to good software development
date: 2022-01-26
tags: ["software", "engineering", "architecture"]
---

Part 6: The hidden answer to good software development

# The series

1. [Part 0: The intro]({{< ref "2020-03-24-the-real-challenges-of-software-engineering-series.md" >}})
1. [Part 1: The categories and common terminology I'll use]({{< ref "2020-03-24-the-categories-of-software-development.md" >}})
1. [Part 2: Marketing and conversation is focused on the wrong areas]({{< ref "2020-03-24-organization-spend-time-in-the-wrong-areas.md" >}})
1. [Part 3: Only good design will save you from these challenges]({{< ref "2020-03-24-only-good-design-saves-you.md" >}})
1. [Part 4: DevOps won't save you from your challenges]({{< ref "2021-02-10-devops-wont-save-you.md" >}})
1. [Part 5: CICD won't save you from your challenges]({{< ref "2021-02-11-cicd-wont-save-you.md" >}})
1. [Part 6: The final answer to good software devlopment (This part)]({{< ref "2022-01-26-the-final-answer-to-good-software-development.md" >}})

# The Intro

I Started writing this blog because I wanted to turn my thoughts into something more organized. I wanted to get them out of my head, so that instead of obsessing over these ideas, I can just put them out there.

It's been good.

Now it's been nearly 4 years since my first post. I think less than 10 people total have even read a single post, but I'm satisfied. I'm happy that I've been able to calm my thoughts a bit.

But there is a problem.

I don't write enough. I have 19 articles right now, including an intro page and a joke post. That's a little over 1 every 3 months. I have no goals to write regularly, only as much as I want, but I feel I need to write more.

I decided to write this article as a final, final post to my long series, [The real challenges in software development]({{< ref "2020-03-24-the-real-challenges-of-software-engineering-series.md" >}}). This is going to create a final question, then answer it, then answer it again.

Why? Because this series is a bit old now, and it needs a more definitive long-lasting answer to the great question. Which question? I'll make that too.

# The final question

After 3 years of writing blogs, I think I have a question. Ready for it?

The question: How do you build a good software development team.

Good is subjective, but in short, it's a team that writes code at a reasonable rate, regularly, can do most anything in their field, and can keep up that coding speed for a long time.

Okay, so let's answer it.

# The final answer

Ready for this? Okay.

Trust your developers.

That's it, just trust them. This actually covers more ground than you might think. For example, if you don't trust them yet, then find people you do. Funny enough, I'm not asking you to force yourself to trust them, I mean, find developers you can trust.

What kind of trust? When you ask the big "Why" question, they should have reasonable answers. Development is hard, but part of our job is to be able to communicate to non-developers. We can't get around it. A good developer knows how to instill trust in the managers around them. A good developer can communicate to other developers that aren't as smart as them. A good developer works on a team. A solo developer can do what they want at home. On a team? They need to be a team member.

What other kind of trust do you want? Someone that takes their time, doesn't rush into answers, is patient. Do you want fast developers? That's good, but someone who does it right, doesn't have to redo it. Going fast isn't worth doing it wrong. Sometimes, there is no right answer, and a good developer will recognize that and get started right away, knowing they can change it later if needed, and that there is no answer in such a case.

You need architects, developers, and interns to have these qualities. Sure, architects will know more than interns, but they all need to be trustworthy, willing to work on a team, willing to compromise for the team.

These are people you can trust.

# The final, final answer.

I don't know. I don't know how to do this. I don't know how to train developers, I don't know the right answers. Software is a young field, less than 100 years old, really. These things are not done being developed. If you have an answer that works today, it might not work tomorrow, sometimes literally.

Oh, but there's more. I have now made a [secret final answer, which should answer more questions in more details]({{< ref "2023-02-19-secret-final-step-to-solve-the-real-challenges-of-software-development.md" >}})

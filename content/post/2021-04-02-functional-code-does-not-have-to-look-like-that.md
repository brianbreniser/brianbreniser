---
title: functional code doesn't have to look like that
date: 2021-03-12
tags: ["functional", "programming", "theory", "random thoughts", "short", "opinion"]
---

Hey, just to let you know, this is my opinion. I don't like how functional languages look, and I feel like we can make c-like languages functional. This is my rant/opinion/feelings. You can feel different if you want! Please do, I may be totally off base. Please don't spam me that I'm wrong, this is just my opinion... Okay, GO!

I really like what functional programming brings to the table, being, the lack of re-assigning variables. The side effect of not being able to re-assign variables, is having more high-level functions that deal with loops, data structures, etc. So, if this is true, doesn't it mean that functional languages can look like any other language? Why do they all seem to copy Lisp?

First I would like to say that the number of parentheses in lisp or lisp-like languages isn't that big of a deal. Ultimately we are comparing this:

`a_function_call(argument);`

To this:

`(a_function_call argument)`

And that isn't any more or less parentheses. Though, yes, lisp also replaces `{}` and even `[]` with parentheses, but I consider all those brackets equally annoying to type. I guess that's why I like the Python/Nim whitespace decision. Less brackets overall. Though, I detest Ruby's decision to use `END`, that just looks weird to me as well.

But really, with so many languages choosing to put parens at the end of function calls, lisp just looks weird, but I know why, it's old. Lisp was built in 1958... WOW! Fortran was built in 1957, and I think it looks way worse. COBOL? 1959, and it looks like hot trash. Lisp 100% won the clean syntax wars in the 50's...

But now, we pretty much settled on c-style syntax. Not to say it's significantly better, but we agree it's 'normal'. For the most part, that's all that matters. Guess what? You can write functional code in these languages too! My favorite example is Rust, if you don't use the `mut` keyword, all variables are static, nothing is mutable, and there, you can write pragmatic functional code (With structs and traits, it's object oriented too! No messy java-like classes either).

This is just a short rant about syntax. I like functional programming, and I want it to be popular in the future, supported by more languages inherently, and performant by default. I just really don't want to write it in a lisp-like syntax.

======= More personal experiences below ======

Further note: I've been experimenting with more functional languages lately. I tried out nim, erlang, and elixir, and holy crap, these are awesome! They all implement functional programming in a different way that I like much better. Let's do a (VERY) quick review of all the functional language syntaxes I've seen:

Lisp: Yuck, parens in the wrong place, everything is a dsl...

Closure: Just Lisp, but runs on the JVM, so it's hard to write, AND it's SLOW! :(

Haskell: Clean function calling, but odd syntax other than that. Too theoretical, great for mathy things, hard for anything practical.


Erlang: Neato, but still too weird to write. Not as experienced with this one.

Elixir: Amazing parallel execution model. Basically the same as erlang other than that. Also `END`!!! What the heck!?

Elm: Nicest front end development language I've ever used, as far as anything FE-only is concerned. I still think writing in literally any language that runs webassembly will be easier, eventually. For now, this is super nice. No one uses it though :(


Nim: Ended with this one because it's not "technically" a full functional language. But like Rust, it does do non-mutable values. Nim has the nicest syntax I've seen, with some quirks, but easily readable. Nim, however, is meant for low-level programming. Actually I think this is the gold standard right now, fast by default, functional for convenience. It does, however, suffer the same everything-becomes-a-dsl problem that Lisp has. I also wish it was more popular, because right now I couldn't find a real job writing Nim yet.

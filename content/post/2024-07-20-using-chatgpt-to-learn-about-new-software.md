---
title: Using ChatsGPT to learn about new software
date: 2024-04-28
tags: ["ai", "gpt", "chatgpt", "tips"]
draft: false
---

# Why talk about this?

A lot has been talked about regarding large language models. Large language models aren't necessarily the best thing with regards to training and search engine replacement, but I have found a pretty good niche use for them that I actually really find very useful.

# Some real advice?

The large language model hype is starting to die down, and a lot of the advice looks pretty much the same. Use it to review things, use it to summarize things, use it to help your grammar, blah blah blah. All of that is pretty obvious at this point.

The real problem is that using large language models to look up things is pretty inconsistent. The only piece of advice I've ever heard that is very useful is only use it to search for things you can easily double check but might have a hard time looking up in the first place. That way you can catch hallucinations easily.

# My use case

Along the lines of that last piece of advice, being able to double check that something exists if the LLM says it exists is a quick way to catch hallucinations. But what are you looking up in the first place that you don't already know about? There's kind of a chicken and an egg problem in there. What kind of thing would you search for that is easy to double check but not easy to find in the first place?

How about software recommendations?

I like to find lesser-known niche products and older products that won't make the front page of Google anymore because there's 300 shiny new ones in its place but those all kind of suck and I think I'd rather have the old one. The old ones are more dependable, have been used for forever, have been vetted, and tend to not show up on search results.

So let's craft a request:

A real request I made recently:

`recommend 10 command-line apps that can take a text file that has lists and sub lists in it in some well-formatted way and turn it into a graphical representation such as a tree`

What, seriously, you're asking an AI to recommend software? That sounds like a terrible idea. How would it know? But here's the trick. Force it to dump many options. 10 was enough in this case, but I've dumped as much as 20, 30, or even 50 before. The goal isn't necessarily to make ChatGPT give you the right answer. It's to give you a bunch of options that you can look up. Oftentimes, you'll recognize some of that software and you can brush it aside. And then there'll be a bunch of names that you don't recognize, and then you just Google those. Oftentimes, ChatGBT will put in a little description of the application, so you can pinpoint the ones that are supposed to do what you really want.

For example, this was the result of that search:

```
1. Graphviz (dot)
   • Description: Graphviz is a powerful tool for creating visual
   representations of structured data. It uses the DOT language to define
   graphs.
   • Command:  dot -Tpng input.dot -o output.png
   • Website: Graphviz http://www.graphviz.org/
 2. Tree
   • Description: Tree is a recursive directory listing command that produces
   a depth-indented listing of files.
   • Command:  tree -L 2 > output.txt
   • Website: Tree http://mama.indstate.edu/users/ice/tree/
 3. D3.js (via Node.js)
   • Description: D3.js is a JavaScript library for producing dynamic,
   interactive data visualizations in web browsers. You can use Node.js to
   run scripts that generate D3 visualizations.
   • Command:  node generateTree.js
   • Website: D3.js https://d3js.org/
 4. PlantUML
   • Description: PlantUML is an open-source tool that allows users to create
   UML diagrams from plain text descriptions.
   • Command:  plantuml input.txt
   • Website: PlantUML http://plantuml.com/
 5. Mermaid CLI
   • Description: Mermaid is a JavaScript-based diagramming and charting tool
   that renders Markdown-inspired text definitions to create and modify
   diagrams dynamically.
   • Command:  mmdc -i input.mmd -o output.png
   • Website: Mermaid https://mermaid-js.github.io/mermaid/#/
 6. Asciidoctor Diagram
   • Description: Asciidoctor Diagram allows you to generate diagrams from
   textual descriptions within Asciidoctor documents.
   • Command:  asciidoctor -r asciidoctor-diagram input.adoc
   • Website: Asciidoctor Diagram https://asciidoctor.org/docs/asciidoctor-
   diagram/
 7. Org-Mode (Emacs)
   • Description: Org-mode is an Emacs mode for keeping notes, maintaining
   TODO lists, planning projects, and authoring documents with a fast and
   effective plain-text system.
   • Command:  emacs --batch -l org input.org -f org-html-export-to-html
   • Website: Org-Mode https://orgmode.org/
 8. DiagrammeR (R)
   • Description: DiagrammeR is an R package that allows you to create graph
   diagrams and visualizations using R.
   • Command:  Rscript -e "DiagrammeR::grViz('input.dot')"
   • Website: DiagrammeR https://rich-iannone.github.io/DiagrammeR/
 9. Treeify (Node.js)
   • Description: Treeify is a Node.js module that converts a nested
   JavaScript object into a tree structure.
   • Command:  node treeify.js
   • Website: Treeify https://www.npmjs.com/package/treeify
 10. MindNode (via Command Line)
   • Description: MindNode is a mind mapping application that can be scripted
   via the command line to generate visual maps from structured text files.
   • Command:  mindnode-cli input.txt -o output.mindnode
   • Website: MindNode https://mindnode.com/
```

Okay, so let's look at this real quick. First things first, it recommended the tree command. Obviously because I said, let's display the results in a tree, and that's a complete waste of time. What the heck is even going on there? That was pointless.

But what is plant UML? Oh, it's an open source tool that allows you to write plain text files and it will just display a prettified version of it. By golly, that was exactly what I was looking for. An old, well-tested tool that takes some well-formatted text and just displays a UI with it.

Mermaid and MindNode were interesting as well. Not exactly what I was looking for, but usable.

Out of all of the weird things I've tried to use a language model for, this has been the most surprisingly helpful. Because each one of these recommendations is an easy Google search, it often includes the website anyway, so I don't even have to search for it, and it's a very small waste of time when it hallucinates.

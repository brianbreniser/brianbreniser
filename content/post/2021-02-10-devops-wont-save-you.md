---
title: Devops won't save you from bad design
date: 2021-02-10
tags: ["software", "engineering", "architecture"]
---

# The series

1. [Part 0: The intro]({{< ref "2020-03-24-the-real-challenges-of-software-engineering-series.md" >}})
1. [Part 1: The categories and common terminology I'll use]({{< ref "2020-03-24-the-categories-of-software-development.md" >}})
1. [Part 2: Marketing and conversation is focused on the wrong areas]({{< ref "2020-03-24-organization-spend-time-in-the-wrong-areas.md" >}})
1. [Part 3: Only good design will save you from these challenges]({{< ref "2020-03-24-only-good-design-saves-you.md" >}})
1. [Part 4: DevOps won't save you from your challenges (This part)]({{< ref "2021-02-10-devops-wont-save-you.md" >}})
1. [Part 5: CICD won't save you from your challenges]({{< ref "2021-02-11-cicd-wont-save-you.md" >}})
1. [Part 6: The final answer to good software devlopment]({{< ref "2022-01-26-the-final-answer-to-good-software-development.md" >}})

(Hey, this article took a year to release! Actually, I wrote it about a year ago, and never got around to proof-reading it until now. Lame, I know. That's just how it goes sometimes. I hope you still enjoy it!)

Check out the [software development categories]({{< ref "2020-03-24-the-categories-of-software-development.md" >}}) before reading this, It's a short read and will make a lot more sense.

# I have the solution!!!

if you've read up on the full series until now, you might be itching and scratching to tell me the answer, the answer is DevOps!

Emphatically no.

# A quick primer

DevOps is an amalgamation of the words "development" and "operations". It's the combination of the developers and operations teams working together to move software forward in a consistent manner. Where the people who write the code and the people who run the code can work in harmony. Also see: DevSecOps, where security is also involved from project conception to production.

# Why it **isn't** the answer

DevOps isn't a tool (We may colloquially refer to it as a 'tool'. But it's a misnomer), more on this later, but rather, it is a theory of better organizational structure. DevOps is a culture. DevOps is a mindset. This sounds like it comes from an all high and mighty philosophical transcendent non-corporeal being who visits us occasionally in this time and space. Jokes aside, some have considered DevOps a silver bullet to _slow_ development. The fact of the matter is that DevOps is given way too much credit for doing way too little. All it really is, is the theory that developers should not throw code over a wall to people who can't throw it back. It's more of a way of organizing teams. It's better to think of DevOps as being a single team having both developers and operators that go to lunch together, and who aren't afraid to say hi in the hallways.

TLDR: If you have a "DevOps team", you aren't doing DevOps right.

If your organization is broken, it will be broken when it adopts DevOps. If you code is broken, your code will be broken under DevOps as well.

Category 4, 5, and 6, problems have nothing to do with how dev an ops runs. It's a code issue through and through.

# DevOps isn't *really* a tool...

*While were on this topic, a bit of a side rant...*

I have a love/hate relationship with tools. Jira, Bamboo, Basecamp, Slack, etc. all have some things in common, they try to help you achieve success in your organization. Sounds great, but unfortunately they are often seen as *THE* way that you adopt some methodology. **Advertisement: Want DevOps? Buy Jira and build a story backlog! Buy Bitbucket to store all your DevOps code! Use Slack to communicate in an *efficient* manner!**.... Blah blah blah.

DevOps is a **process**, it's a **culture**. Can you do DevOps without Jira? Of course! Can you write your backlog on a white board! Yes! Can you keep a git server that *isn't* on a "hosted service"? Yes! Can you use IRC to talk? Or you know, just talk to each other? **EMPHATICALLY YES**. DevOps isn't a *tool*.

TLDR. If you purchased the Atlassian stack to "do DevOps", you aren't "doing DevOps".

/rant

# But you didn't mention containers, docker, kubernetes, or other DevOps things!!

You don't need them to have DevOps. _DevOps aren't tools_. DevOps aren't services. DevOps isn't something you buy. It is a culture, and a difficult culture to adopt, because it's hard to change old habits.

Neither those tools, or DevOps, will help you write software. It can help you *run* software, or *release* software, in theory, if you're app is malleable enough to fit that culture. The point is that you can't write code easier by adopting DevOps, or any "DevOps tools", or any services associated with DevOps.

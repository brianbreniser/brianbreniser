---
title: About me
comments: false
---

I'm a software developer by trade and practice. I get antsy when I haven't programmed in awhile. My expertise in software development has grown throughout the years, and I feel confident in my abilities.

I'm a generalist, this means I don't have full expertise in one programming language/framework/platform or another. I feel that, for me, it's best to keep my eye on many systems, and be able to flow through them as best as I can. As a consultant, I have experience doing this. I moved from company to company, working on different systems, many times a year. In my experience, a good team needs both generalists and specialists. This allows for finding solutions in very specific domains, as well as thinking outside the box.

As a counterexample, you can't be a generalist at everything, and regarding operating systems, I'm definitely most familiar with Linux. I prefer the Gnu/Linux OS above others because, ultimately, most cloud based systems run on Linux, so my expertise on the command line can be applied to running systems. Running containers is native to Linux, and code that I write and test locally runs cross-platform, which is very convenient. Lastly, Linux has allowed me to customize my workflow to best suite my development style.

Outside of work, I have a wonderful family, my wife and baby help complete me as a well rounded person. I enjoy every minute I get to have with my son, to watch him grow and learn. It's exciting to watch a new life enjoy things for the first time.

I'm a science buff as well, I enjoy reading about space, technology, physics, chemistry, math, and biology. I have a myriad of science facts crammed into my head. I'm a fantastic addition to a trivia team, where I can focus on science questions, while others can take care of pop culture and arts.

My other hobbies include practical woodworking. I've built simple cabinets and multiple built-in shelves, and custom closets for our house. I don't have time to experiment with fancy woods, but I'm a very practical builder that can build high quality and long lasting things, so long as it can be practical and not need to be very pretty.

I find my science and woodworking hobby to assist me in my software development on occasion. As a generalist, I also think that seeing your project from a new perspective can bring positive changes. Though these are all separate interests, I find they have more in common than meets the eye.

Thanks for taking the time to read about a random stranger on the internet. If you are interested in talking more, possibly about job opportunities, then my resume, linked above, is a great next step.


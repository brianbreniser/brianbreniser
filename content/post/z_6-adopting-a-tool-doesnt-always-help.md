---
title: Adopting a tool doesn't always help
date: 2020-04-16
tags: ["software", "engineering", "tools"]
draft: true
---

Idea: When a company picks up a tool, then just abuses it as before, they might get a false sense of progress by taking no steps forward.

Example:
. Cayuse adopting AWS lambdas because it was the "future". It was "cheap". It also made "microservices easy". As well it made "scaling horizontally" easy.
. Lambdas are constantly talked about as forcing developers to write functional code, stateless, etc. But then, they threw multiple services in it and created a monolith with spaghetti code, all in one serverless function.

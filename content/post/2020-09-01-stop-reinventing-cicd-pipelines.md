---
title: Stop Reinventing Pipelines
date: 2020-09-01
tags: ["from the heart", "software", "cicd", "pipelines", "architecture"]
---

If you are familiar with [clean architecture by Bob Martin](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) or [Any layered architecture in general](https://en.wikipedia.org/wiki/Multitier_architecture) you are familiar with layers of abstraction. Layers of abstractions are a fantastic way to write code now, without a concrete implementation in mind, test it, make the logic work, and implement the concrete code later. I love having the ability to do this. It's just a fantastic feeling writing code that you *know* will work, that you **know** does what you want, and any issues that come up later are with the integration of the two pieces, not your logic.

But so often we are sidetracked by "getting things to work". It is an obsession in new projects to get a hello page working, get a service running with only a hello endpoint working, to get a database setup with nothing in it. We celebrate getting the database to work! Why? Because it's hard! We *like* seeing things run, we *like* successfully running things.

CICD pipelines end up in the *just get it done* category. We don't really think about how to write them well. Let's think about it for a minute though, how long will our pipeline exist? **As long as our application does!**. How long have you been on your current build system? Check out the [wikipedia page on build systems](https://en.wikipedia.org/wiki/List_of_build_automation_software). Just look at the continuous integration section, 15 tools, and the hosted, modern CICD pipelines are all built in the last 10 years. Some of the older ones are language-specific. Do you know how many build systems you might go through within the lifetime of your application? 3? 5? 10? How many awesome shiny things will come along we will want to use? We should probably consider writing our build systems in a way that WE control them, not our build systems.

How does <***insert build pipeline***> work? Well, we have <***series of steps***> that we need to do, so we open up <***the proprietary editor***> and put in our <***business logic***>. Then, one step at a time, <***insert build pipeline***> runs each step and <***Build and deploys the application***>.

Can this be abstracted? **YES!**. Insofar as you can abstract out **HOW** to **RUN** your build. This is a bit of inversion of an abstract architecture. We will build the concrete code (Because build systems ONLY work concretely), but we can use ANY **build system** in an abstract way. Let's see an example:

We have a Java service built to run on Linux, we should be able to run on any Linux implementation, so that is kind of abstract. Let's say we use some tooling to test/scan/copy/deploy/etc that are available on Linux, how can we implement that? In Jenkins, you can use the Jenkins pipeline syntax, or Bamboo build steps, or <***insert build system here proprietary blah blah***>... ***NO***, let's write this in something that can move around, like... Just Bash scripts?!?

Swallow that vomit, because everything old is new again! A GUI isn't always better than a terminal! Websites **DON'T** make great app platforms, and Monoliths are coming back to replace microservices again! Sometimes, the simplest tool was the best tool the whole time.

You can run scripts anywhere you have Linux, you can install dependencies anywhere you have a package manager, and compile Java anywhere that package manager has a Java compiler. Linux scripts are the absolutely best way to get a flexible build environment. Want to build locally? just run `./build.sh`... Want to build in Jenkins? Have the Jenkins pipeline run `./build.sh`. Bamboo? `./build.sh`. You know what? It would be the same for *any new build environment ever created that has Linux running*. If you did this 5 years ago, this would work for every build system up to and including Tekton, the latest build system for Kubernetes/OpenShift.

Want one better? Containers are a godsend for so many reasons, they are certainly not over-hyped, and no matter what happens in the future, I predict containers will be a part of that journey. Containers are part of the Linux kernel, so they aren't going away anytime soon. So why not run our build system in a container? Well, news flash, you probably already are! Jenkins can run containers, Bamboo can run containers, and Tekton is **ONLY** containers.

Now you can use Bash, Python, Ruby, Csh, podman, skopeo, Java, Javascript, COBOL, maven, sonar, any scanning tool, curl, REST api's, literally *anything* that you can find in your containers package manager can be used or built up to be used to build, test, scan, deploy, and auto-configure your project.

***ANYTHING***

***A-NY-THING***

*Any* system that runs containers can now be used to run *any* build system. Jenkins doesn't support *X*? If your container has it, now it *does* support *X*! Anything that runs on Linux now runs in your build system. You don't need Jenkins's permission, you don't have to be limited by Bamboo, and you don't have to wait for Tekton to support something to do *anything* you want to do.

Too much freedom? Welcome to software development! It's hard, but limiting your options isn't doing anyone any good. Yes, using containers is freedom beyond freedom, and it is hard to know where to start. Start small and build up. Doing things like this is what being a developer, being an *engineer*, is about. We do the hard things because we are *smart*, *trained*, and we are *skilled* in our field.

Imagine a build system that looked like this:

```groovy
pipeline {

    agent custom-agent

    stages {

        stage("Get code") {
            steps {
                sh `<build system here> <command>` // git, svn, mercurial? All of them work! No plugin needed...
            }
        }

        stage("Switch to branch $branch") {
            steps {
                sh `./switch_branch $branch` // Same here, once you have your build scripts from your source control, you can do anything
            }
        }

        stage("Build it") {
            steps {
                sh `./build.sh` // Mvn? good to go! Javascript? Good to go! Go? Good to... Go!
            }
        }

        stage("Test it") {
            steps {
                sh `./test.sh` // Any test system works, any custom test framework works, no plugins necessary. What if tests fail? Return non-zero exit codes, notify slack channels, email or text the developer, you can do anything here you need.
            }
        }

        stage("scan it") {
            steps {
                sh `./scan.sh` // Which scanning system? All of them? Any of them? You decide!
            }
        }

        stage("Notify people") {
            steps {
                sh `./notify.sh` // Need people notified? You can send it anywhere! Slack? IRC? Text message? Yes!
            }
        }

        stage("Deploy to $arg1 $arg2") {
            steps {
                sh `./deploy.sh $arg1 $arg2` // You decide how to deploy, what arguments does your build script need? Write it! Your build system can forward arguments, you can configure it however your build system needs, and you can use those arguments however you need.
            }
        }

        stage("Test deployment") {
            steps {
                sh `./integration_tests.sh` // With shell scripting, interpreted languages, curl, a package manager, etc, you can run any test that you can execute from the command line! Even headless browser testing!!!
            }
        }

        stage("Promote $arg1 $arg2") {
            steps {
                sh `./deploy.sh $arg1 $arg2` // When all goes well, you can promote it. How? The details don't matter here!
            }
        }

    }

}
```

Same for Any build system, Tekton?

Task:

```yaml
apiVersion: tekton.dev/v1beta1
kind: Task
metadata:
  name: build-it
spec:
  steps:
    - name: build-it
      image: custom-image
      workingDir: $cicd_scripts_directory
      script: |
        ./build.sh
```

Similar for test, scan, notify, deploy, integration test, and promote

Pipeline:

```yaml
apiVersion: tekton.dev/v1beta1
kind: Pipeline
metadata:
  name: xyz-project-build-pipeline
spec:
  resources:
    ...
  tasks:
    - name: build-it
      taskRef:
        name: build-it
    - name: test-it
      taskRef:
        name: test-it
... And so on, similar in fashion to the other example ...
```
With this kind of system, you can introduce a new build system in 2 steps

1. Learn how the system runs.
1. Learn the syntax.
1. Learn how to run Linux scripts (or containers) with arguments/environment variables, and other resources needed
1. Debug becuase systems lie and documentation is outdated
1. Done, you've moved your build system from `X` to `Y` and you ***didn't need to rewrite your core logic***

Any build system gotchas might get in the way, but those gotchas will always be around, you have to work around them anyway.

In short, why would you build your pipeline in something you do *not* control? I would rather have a full feature set, more control, and a tried and tested system for my build system. If I need to "bash" Jenkins or Tekton around to fit my needs (Pun intended), I can.


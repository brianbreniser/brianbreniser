---
title: If Working on A Legacy App Was Like Working on A Car
date: 2019-10-24
tags: ["freethought", "funny"]
---

### Disclaimer:
* The opinions expressed here are a joke, not to be taken too seriously. They express a worst case scenario and not necessarily any real life software or car. They do not necessarily directly represent the work the author has done in their industry, it is a combination of some experiences and a lot of [the daily wtf](https://thedailywtf.com/). With that out of the way, I hope you found this funny!

### Congrats on the new job!
You are the newcomer to a successful company that has a legacy car you will be working on. On your first day you take yourself on a little tour of the car.

You first notice a couple of clashing design perspectives. It appears the body is made of a mixture of aluminum, plastic, glass, and what you think is drywall and counter top tiles. The body panels looks like they are stitched to together with sewing thread.

Regarding the core architecture, you notice that there are 3 completely different engines for the car. A gas motor pointing in the x direction, a diesel pointing in the y direction, and an electric motor pointing in the z direction. The battery is dead so the electric motor doesn't work, but you can't take it out or else you think the other 2 engines will fall apart. All of this doesn't matter because none of these motors move the car forward, so somebody strapped a jet engine to the back of the car with duck tape. You can't replace either gas based motor because they run different parts of the electronics of the car.

In terms of the user interface, in the cockpit there are 2 steering wheels and a joystick. The joystick seems to turn the car right, and the right steering wheel seems to turn the car left. Curiously the left steering wheel doesn't turn anything, but accelerates the jet engine. Which is a good thing because none of the 4 pedals in the car accelerate anything, though the leftmost pedal will honk the horn if you step on it 3 times in rapid succession on every 4 attempts.

Actual users tend to find the bucket turned over in place of the front seat very uncomfortable, so some users demand that for their version of the car, it will be driven by a person sitting on top of the car with rope attached to the inner control panels of the car. The ropes are not labeled and are often knotted together, may users are confused, and your call center is constantly inundated with user errors that are labeled in the ticket system as ["PEBCAK"](https://en.wiktionary.org/wiki/PEBCAK) and ["ID 10 T"](https://en.wiktionary.org/wiki/ID10T#English) Errors.

### Your first ticket

You are tasked with changing the breaks. There are no instructions to the car, nor are there any tires, or break calipers. You aren't sure why the breaks need to be changed, but your superiors insist the change must be made. They recommend you go through the trunk, as the last person who changed the breaks was back there an awful long time, though nobody knows exactly why, and they left the company 3 years ago, so you can't ask them how they did it.

You are pretty sure the "breaks" that they were talking about were actually the fuel tank, and the reason you have to go in through the trunk is that someone welded steel around the bottom of the car, presumably to hold in the pieces that are falling off, which is basically all of them. After 2 weeks of trying to change the "breaks", you are pretty sure you discovered that the car leaks jet fuel. You are not totally sure, because when you patch the leak, the joystick stops turning the car right. When you bring up to the more experience mechanics that patching the fuel line makes the joystick stop working, they tell you that is a known issue because of the incompatibilities of that version of the fuel line and the turning mechanism, and tell you to install another fuel line separate from the leaking one. You are pretty sure there are 4 there already, but a 5th won't hurt anything, so you spend a month adding it.

### Everything is a nail

After you've been with the company awhile (Your resume has been updated and recruiters are no longer being ignored) you are called in for a product design meeting. After speaking with the customer relations manager, you learn that the customer is asking for the car to be completely rebuilt from the ground up. They claim that it needs to "Look exactly like a bicycle". Unfortunately, what they describe is actually a sled, but claim that it should look exactly like a bicycle. They do insist, however, that it needs to function well for going down snow covered hills. Your team begins the process of building a new fleet of custom airliners to sell to the customer, and the bill justifies all the time spent on jet fuel installations.

---
title: The 5 Levels of Software Engineering
date: 2018-03-26
tags: ["software engineering"]
---

Edited 2019-10-24 for spelling, grammar, and better clarifications.

## Why does this exist?

There are a ridiculous number of ways to slice up learning
how to go from "interested in this thing called coding" to "actually being a full time developer".
I'd like to go through my opinion on these steps after reflecting on the journey I took to get where I
am as a developer. If you like these levels of abstraction, you can use them to judge.... ahem,
I mean think about how yourself or others progress through learning about software development. Also,
these 5 steps are not meant to be an exhaustive list of steps for your whole career, (such as the 5 levels of mastery)
but merely a mental model to move from "I can program on my own computer for my own amusement" 
to "I get paid to help a team of people bring a product to market".

**Disclaimer**

This article really only follows a typical app developer going through school (or self taught) and
working for a company building apps that they sell for a profit. These levels don't apply as well
to open source developers (major respect given to those people!), game developers, people working on simple command line apps, data researchers, etc.

## The levels by name

levels:

* Level 0: Regurgitating what someone tells you to write

* Level 1: Being able to build an app in one language, on one framework, or using one kind of abstraction

* Level 2: Experienced branching out of your one framework or language, using different levels of abstraction

* Level 3: Succeeding in being handed a new framework you didn't plan to use, and becoming proficient in it

* Level 4: Holistic experience building apps in arbitrary frameworks across a big team (actual software engineering)

## TLDR of the level details

### 0: Regurgitating what someone tells you to write

1. You can only write code with specific direction
1. You have barely learned, or just now are learning, loops, functions, classes, functional paradigms
1. You are in the first few classes of a CS degree, early in a code school, or just began programming in your free time

### 1: Being able to build something in one language, in one framework, or using one kind of abstraction

1. You have built one "app", defined pretty broadly
1. You have an understanding of requirements, development, and bug fixes
1. You have finished a project you started

### 2: Experienced branching out of your first framework, using a higher level of abstraction

1. You have done everything in level 1, but in a new language or totally different framework or model
1. Not a "required" step to get to level 3, but a pretty nice step before heading into level 3
1. At this point you are close to graduating from a school (or have already), and are looking for internships or junior developer jobs

### 3: Succeeding in being handed a new framework you didn't plan to use, and becoming proficient in it

1. You have done mostly everything again from level 1 and/or 2, but you didn't choose what you used
1. This is an important step for app development to allow you to be flexible in your work (and to work for someone else)
1. This might be the first particularly challenging step in your new career
1. You have probably finished an internship or been a junior developer awhile

### 4: Holistic experience building apps in arbitrary frameworks across a big team (actual software engineering)

1. You have a good, broad understanding of software development as a skill
1. You understand that software development is about more than code, it's about teams

## The levels in detail

### 0: Regurgitating what someone tells you to write

##### Explanation

At the 0th level of software development, you have gone from not writing a single line of code, ever,
to being able to write what somebody tells you to write, nearly verbatim. This kind of programming is the
first step in learning how to code, and is unavoidable. You must at some point go from knowing nothing, to
reading, or being taught, your first few lines of code, and simply regurgitating that information back.

#### Difficulty

Very easy. This level of programming can be taught to a child of nearly any age, it does not require any real
understanding of programming, app building, frameworks, or anything beyond copy/paste.

#### What can you do at this level?

Nothing, really. However, This is a very important level, as with learning any skill, you are required to
get past the knowing *nothing* stage, to actually knowing *something*, even if you have no proof
you will be good at it yet.

#### Benefits of making it this far

You learned a thing! Even if you don't make it past this point, you can now tell someone you've
learned, on some level, what coding really is. You might be able to recognize jokes in movies that relate
to coding, you can read code (even if you don't fully understand it), and you understand your
calculator a bit better. The benefits of this level of coding is the same as being exposed
to anything else at this level, you have expanded your horizons a bit to something new
potentially outside your comfort zone, congratulations!

#### Who is at this stage?

Computer science students in their very first couple classes, a code schooler in the beginning of their program,
someone who has never programmed before following a tutorial that hand-holds them through the process.

### 1: Being able to build in one language, on one framework, or using one kind of abstraction

##### Explanation

You have built an app! It doesn't have to be terribly useful, just *something* that works.
Even if you re-implement todo apps, or note taking apps, or a really simple video splicer. You have come far enough
that you have built, from start to finish, a product that you, or someone else, might be able to use. This category is
really general on purpose. I don't prescribe the kind of thing, because this level is more about being able to see
something through to the end. Even if it's just a website (that you built from scratch more than just copy/pasted)
that displays your computers stats in a friendly format (or many computers stats, if you need that in a devops or
admin kind of way).

#### Difficulty

Still pretty easy, but much harder relative to level 0. This is mostly difficult because it
combines multiple pieces of development together. 1) knowing how to code, 2) knowing how to build a front end (even
if that is just a command line, or a useful api to be consumed by others), 3) having requirements (even if they are only for your own
needs), and 4) being able to find and squish bugs, even fairly trivial ones.

#### What can you do at this level?

Build something useful! For this first time, you can make a website for your company (A small company that won't
outsource that kind of thing), you can build an app that you use on a daily basis, or maybe even something someone
else will find useful.

#### Benefits of making it this far

You have seen a project through to the end. It takes dedication beyond just casual interest to build something
from start to finish. Real congratulations! You have proven to others that you, in some capacity, can build an app
on your own.

#### Who is at this stage?

Most CS students make it past this point in their intro to software development class. One could argue that
because the teacher is hand holding them through the process, this is little more than level 0, but my argument is
that level 1 is still pretty open to interpretation, if you built anything from start to finish that could be
useful, it counts. Code schoolers should get past this point as well. If you are learning programming on your own,
you can get past this point, but without direction it will be harder (though, arguably, you could learn more
from your mistakes. Feel good if you made it this far without teachers!).

### 2: Experienced branching out of your one framework, using different levels of abstraction

##### Explanation

Essentially, this is the same amount of work as level 1, but you've built something in a second framework, different from the one you have experience
in when you were learning to program in the first place. Maybe instead of a website, you built a command line app
or visa versa. Maybe you learned a new language, and built an app in that. Maybe you learned a whole new framework
in your current language, that does things totally differently. The Idea here is you have some breadth of experience,
you now understand not everyone builds apps the same way you learned. Bonus points if you moved between
an OOP language and a Functional language (because you really got a shock in a different way to program)

This stage is not *required* to move on to the next stage. In some cases, the second thing you learn might come
in the form of someone else requiring you to work on it. Bear in mind that skipping this stage will make
level 3 much harder, but technically it's not required.

#### Difficulty

Basically the same as level 1, but the difficulty is mostly in starting over with a new language, or a totally
new way of programming in your personal history.

#### What can you do at this level?

Same as level 1, but you can build it in two languages now.

#### Benefits of making it this far

You have learned that the way you learned to program isn't the only way. There are many people in this world
who will learn programming, app development, websites, or whatever, differently from you. You have branched out
of your comfort zone at least once, and have seen the grass on the other side.

#### Who is at this stage?

Most CS students will get to this point before they graduate if they challenge themselves to learn things outside the classroom. From what I've
heard, a code school will not get you to this point, as most of them only teach you one language and one framework,
but you could get lucky and get a school that teaches you alternative methods of development as well. If you are
learning programming on your own, you are getting quite far if you make it to this point, as you have decided
to check out the other side of the fence yourself, which is difficult to do without direction.

### 3: Succeeding in being handed a new framework you didn't plan to use, and becoming proficient in it

##### Explanation

This level is a special one, because it finally addresses a key aspect of programming professionally
for a company, namely, that you might be using a language or framework you didn't choose to learn. If this
happens, and you embrace it and successfully work on it as well (or nearly as well) as anything else
you've worked on, you will have passed a major milestone in real word development.

As a sub-level 3 category, you might have to deal with a legacy app. This is also very important
but I'm keeping it with level 3 because I didn't feel there was linear enough progression to put it
in it's own category. One thing about being forced into a language/framework you didn't plan on
using is that most likely it will be a legacy app, as by the time you've seen level 2, you've probably
at least explored *something* about all "modern" languages and frameworks, and at least dabbled a bit
in them. If you are given something you've never considered working on before, it's probably because it's old enough that it's not talked about anymore.

#### Difficulty

Somewhere between very hard and a deal breaker for some people, depending on your prior experience and ability to accept the new
language or framework. Getting passed this point is a bit of a make it or break it point in your
professional career because most companies don't care that you know **some** language, but that you
can learn **any** language in general. This means there is a good likelihood that you will end up working in a language/framework you didn't choose as part of your career. You might even find what you want to work on is not even really
offered as a full time job (Yet) (**Cough cough, Rust, cough cough**).

#### What can you do at this level?

Congratulations, you can now get a full time job almost anywhere (Software engineer level 1 or equivalent, no more internships or junior dev positions). At level 2 or lower, you had a very
small selection of jobs you could really be good at, as it required you worked on a limited subset
of languages or frameworks. At this point, you can now pick up nearly any job nearly anywhere (within
a particular field of development, within reason) and be good at it.

#### Benefits of making it this far

Marketability, able to get and keep a job, able to make a good living working as a software developer.
Every level before this point was mostly just messing around, or were applicable to pet projects (not a
bad thing, just more limited by definition). Now, you can call yourself a professional developer, and
you can embrace and work on nearly anything.

#### Who is at this stage?

No matter where you get your education from, almost anyone will get past this stage if they get past an internship or junior development job. 4 year degree schools won't get you here in most cases. Code schools stop short of this as well, and when learning to code on your own you will likely
find it's hard to work on something you didn't choose on some level.

### 4: Holistic experience building apps in arbitrary frameworks across a big team (actual software engineering)

##### Explanation

This is where the journey really begins. In this model, starting level 4 work is the core of your professional
career, and finishing level 4 is, on some level, impossible, as you can always get better at the holistic
experience of developing software. The model does not distinguish the nearly infinite little things you could
learn when in level 4, and it does not take into account work outside of app development. I still find this model
useful, as it encompass the quick development of someone going through school or learning code for the first
time. At this point, level 4 is just a continuous journey of learning about teamwork and process. By this time
you should be proficient in programming, and can take on pretty arbitrary challenges.

#### Difficulty

Very hard, not acquired by anyone but professional developers who code for a career (Paid or not). The difficulty is similar to any professional engineering degree.
Professional degrees in the fields of engineering, nursing, counseling, or other STEAM fields, are of similar difficulty.
Attaining this level is probably easier than some degrees that require even more school before graduating with a degree, such as doctors or lawyers.
But on the whole, it is a great achievement in a career.

#### What can you do at this level?

You can rise through the ranks as a software developer. At this level you are proficient enough to work nearly anywhere, and you will be paid well to do so.
At this point you could move into being an application architect, split your skill set and reach a similarly difficult level in system architecture.
You could also, if you so choose, decide to spend your time consulting, teaching, and guiding others to be as proficient as yourself.

#### Benefits of making it this far

Congrats, you have moved swiftly through learning how to be a software developer, but you are not done yet, your
career really starts now, and you can hone your technique to be better and better. Level 4 is super broad, so benefits
vary greatly based on how far you can take it. Benefits include being paid well to being a top performer, being promoted to architectural jobs, changing your careers and leveraging you background to become a manger, director, or CTO. Starting from this point you can branch off to any part of software development you would like.

#### Who is at this stage?

Anyone working as an SE 1 or equivalent software developer should be close to this level (if they are not, they might
have been promoted too early, in my opinion), but this level encompass any developer through the typical software engineering levels (Such as SE 1 - 5), software architect, development team lead, etc. Level 4 just encompass the idea that you made it past the traditional schooling/internships, and you can now begin your lifelong journey of learning ever more about software engineering. At this point it's probably more useful to track progression with a more generally applicable scale, such as the 
[Dreyfus model of skill acquisition](https://en.wikipedia.org/wiki/Dreyfus_model_of_skill_acquisition), which is
a general model of skill acquisition, but has better details on your relative experience to others.
